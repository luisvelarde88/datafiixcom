<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::group(['middleware' => 'prevent-back-history'], function () {
    Route::get('activate/{token}', 'ActivateController@userAccount');

    Route::get('user/resetPasswordAccount/{token}', 'ActivateController@userAccount');

    Route::post('user/resetPasswordAccount', 'UserController@resetPasswordAccount');
});
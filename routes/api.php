<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('hola', 'prueba@hola');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware'=>'auth:api'], function(){

    //para las cuentas SMS
    Route::get('cat_cuentas_sms','cat_cuentas_smsController@getAll')->name('getAllcat_cuentas_sms');
    Route::post('cat_cuentas_sms_fil','cat_cuentas_smsController@getfil')->name('getfilcat_cuentas_sms');
    Route::post('cat_cuentas_sms_add','cat_cuentas_smsController@add')->name('adicionarcat_cuentas_sms');
    Route::get('cat_cuentas_sms/{id}','cat_cuentas_smsController@get')->name('getcat_cuentas_sms');
    Route::get('cat_cuentas_sms/delete/{id}','cat_cuentas_smsController@delete')->name('deletecat_cuentas_sms');
    Route::post('cat_cuentas_sms/edit/{id}','cat_cuentas_smsController@edit')->name('editcat_cuentas_sms');

    //para los eventos
    Route::post('eventos','EventosController@getCuentafil')->name('Eventosfil');
   
    Route::post('eventosfil','EventosController@Cuentafil2')->name('Eventosfilu');
    Route::post('eventosfil/{offset}','EventosController@Cuentafil3')->name('Eventosof');
    Route::get('eventos/{id}','EventosController@get')->name('Eventosfilu');
    //Route::get('eventos2/{id}','EventosController@get')->name('Eventosfilu');
    Route::post('eventosPruebas','EventosControlle_copyr@getCuentafil')->name('Eventosfil');
    Route::post('eventosfilPruebas','EventosController_copy@Cuentafil2')->name('Eventosfilu');

   // Route::post('eventosfil2','EventosController@Cuentafil2')->name('Eventosfilu2');
   



    


   ///COLONIAS
   
   Route::post('cat_colonias_fil','Cat_coloniasController@getfil')->name('getcolonias_fil');
   Route::post('cat_colonias_add','cat_coloniasController@add')->name('adicionarcat_colonias');
   Route::get('cat_cuentas_sms/{id}','cat_cuentas_smsController@get')->name('getcat_colonias');
   Route::get('cat_colonias/delete/{id}','cat_coloniasController@delete')->name('deletecat_colonias');
   Route::post('cat_colonias/edit/{id}','cat_coloniasController@edit')->name('editcat_colonias');

    // CEDULAS

    Route::post('cat_cedulas', 'cat_cedulas_controller@add')->name('Cedulas_Add');
    Route::get('cat_cedulas', 'cat_cedulas_controller@getAll')->name('Cedulas_All');
    Route::get('cat_cedulas/{id}', 'cat_cedulas_controller@get')->name('Cedulas_Get');
    Route::post('cat_cedulas/filter', 'cat_cedulas_controller@filter')->name('Cedulas_Filter');
    Route::put('cat_cedulas/{id}', 'cat_cedulas_controller@edit')->name('Cedulas_Edit');
    Route::delete('cat_cedulas/{id}', 'cat_cedulas_controller@delete')->name('Cedulas_Delete');

    // CODIGOS

    Route::post('cat_codigos', 'cat_codigos_controller@add')->name('codigos_Add');
    Route::get('cat_codigos', 'cat_codigos_controller@getAll')->name('codigos_All');
    Route::get('cat_codigos/{id}', 'cat_codigos_controller@get')->name('codigos_Get');
    Route::post('cat_codigos/filter', 'cat_codigos_controller@filter')->name('codigos_Filter');
    Route::put('cat_codigos/{id}', 'cat_codigos_controller@edit')->name('codigos_Edit');
    Route::delete('cat_codigos/{id}', 'cat_codigos_controller@delete')->name('codigos_Delete');

    // CODIGOS DETALLES

    Route::post('cat_codigos_detalles', 'cat_codigos_detalles_controller@add')->name('codigos_detalles_Add');
    Route::get('cat_codigos_detalles', 'cat_codigos_detalles_controller@getAll')->name('codigos_detalles_All');
    Route::get('cat_codigos_detalles/{id}', 'cat_codigos_detalles_controller@get')->name('codigos_detalles_Get');
    Route::post('cat_codigos_detalles/filter', 'cat_codigos_detalles_controller@filter')->name('codigos_detalles_Filter');
    Route::put('cat_codigos_detalles/{id}', 'cat_codigos_detalles_controller@edit')->name('codigos_detalles_Edit');
    Route::delete('cat_codigos_detalles/{id}', 'cat_codigos_detalles_controller@delete')->name('codigos_detalles_Delete');

    // CUENTAS

    Route::post('cat_cuentas', 'cat_cuentas_controller@add')->name('cuentas_Add');
    Route::get('cat_cuentas', 'cat_cuentas_controller@getAll')->name('cuentas_All');
    Route::get('cat_cuentas/{id}', 'cat_cuentas_controller@get')->name('cuentas_Get');
    Route::post('cat_cuentas/filter', 'cat_cuentas_controller@filter')->name('cuentas_Filter');
    Route::put('cat_cuentas/{id}', 'cat_cuentas_controller@edit')->name('cuentas_Edit');
    Route::delete('cat_cuentas/{id}', 'cat_cuentas_controller@delete')->name('cuentas_Delete');
    Route::post('cat_cuentas/filtro', 'cat_cuentas_controller@filtro')->name('cuentas_Filtro');
    Route::post('cat_cuentas/filtro/{id}', 'cat_cuentas_controller@filtroNot')->name('cuentas_FiltroNot');
    Route::get('cat_cuentas_cuentas/{id}', 'cat_cuentas_controller@getcuenta')->name('cuentas_cuentas');
    // CUENTAS WHATSAPP

    Route::post('cat_cuentas_whatsapp', 'cat_cuentas_whatsapp_controller@add')->name('cuentas_whatsapp_Add');
    Route::get('cat_cuentas_whatsapp', 'cat_cuentas_whatsapp_controller@getAll')->name('cuentas_whatsapp_All');
    Route::get('cat_cuentas_whatsapp/{id}', 'cat_cuentas_whatsapp_controller@get')->name('cuentas_whatsapp_Get');
    Route::post('cat_cuentas_whatsapp/filter', 'cat_cuentas_whatsapp_controller@filter')->name('cuentas_whatsapp_Filter');
    Route::put('cat_cuentas_whatsapp/{id}', 'cat_cuentas_whatsapp_controller@edit')->name('cuentas_whatsapp_Edit');
    Route::delete('cat_cuentas_whatsapp/{id}', 'cat_cuentas_whatsapp_controller@delete')->name('cuentas_whatsapp_Delete');

    // ESTADOS

    Route::post('cat_estados', 'cat_estados_controller@add')->name('estados_Add');
    Route::get('cat_estados', 'cat_estados_controller@getAll')->name('estados_All');
    Route::get('cat_estados/{id}', 'cat_estados_controller@get')->name('estados_Get');
    Route::post('cat_estados/filter', 'cat_estados_controller@filter')->name('estados_Filter');
    Route::put('cat_estados/{id}', 'cat_estados_controller@edit')->name('estados_Edit');
    Route::delete('cat_estados/{id}', 'cat_estados_controller@delete')->name('estados_Delete');

    // IDIOMAS

    Route::post('cat_idiomas', 'cat_idiomas_controller@add')->name('idiomas_Add');
    Route::get('cat_idiomas', 'cat_idiomas_controller@getAll')->name('idiomas_All');
    Route::get('cat_idiomas/{id}', 'cat_idiomas_controller@get')->name('idiomas_Get');
    Route::post('cat_idiomas/filter', 'cat_idiomas_controller@filter')->name('idiomas_Filter');
    Route::put('cat_idiomas/{id}', 'cat_idiomas_controller@edit')->name('idiomas_Edit');
    Route::delete('cat_idiomas/{id}', 'cat_idiomas_controller@delete')->name('idiomas_Delete');

    // LOCALIDAD

    Route::post('cat_localidad', 'cat_localidad_controller@add')->name('localidad_Add');
    Route::get('cat_localidad', 'cat_localidad_controller@getAll')->name('localidad_All');
    Route::get('cat_localidad/{id}', 'cat_localidad_controller@get')->name('localidad_Get');
    Route::post('cat_localidad/filter', 'cat_localidad_controller@filter')->name('localidad_Filter');
    Route::put('cat_localidad/{id}', 'cat_localidad_controller@edit')->name('localidad_Edit');
    Route::delete('cat_localidad/{id}', 'cat_localidad_controller@delete')->name('localidad_Delete');

    // MUNICIPIOS

    Route::post('cat_municipios', 'cat_municipios_controller@add')->name('municipios_Add');
    Route::get('cat_municipios', 'cat_municipios_controller@getAll')->name('municipios_All');
    Route::get('cat_municipios/{id}', 'cat_municipios_controller@get')->name('municipios_Get');
    Route::post('cat_municipios/filter', 'cat_municipios_controller@filter')->name('municipios_Filter');
    Route::put('cat_municipios/{id}', 'cat_municipios_controller@edit')->name('municipios_Edit');
    Route::delete('cat_municipios/{id}', 'cat_municipios_controller@delete')->name('municipios_Delete');

    // TECNICOS

    Route::post('cat_tecnicos', 'cat_tecnicos_controller@add')->name('tecnicos_Add');
    Route::get('cat_tecnicos', 'cat_tecnicos_controller@getAll')->name('tecnicos_All');
    Route::get('cat_tecnicos/{id}', 'cat_tecnicos_controller@get')->name('tecnicos_Get');
    Route::post('cat_tecnicos/filter', 'cat_tecnicos_controller@filter')->name('tecnicos_Filter');
    Route::put('cat_tecnicos/{id}', 'cat_tecnicos_controller@edit')->name('tecnicos_Edit');
    Route::delete('cat_tecnicos/{id}', 'cat_tecnicos_controller@delete')->name('tecnicos_Delete');

    // ZONAS




    //push_devices
    Route::post('push_devices', 'push_devicescontroller@add')->name('push_devices_Add');
    Route::post('push_devices/{id}', 'push_devicescontroller@edit2')->name('push_devices_edit');
    Route::post('push_devices/updateDataDevice/{id}', 'push_devicescontroller@edit_push_devices')->name('updateDataDevice');
    Route::get('push_devices/{id}', 'push_devicescontroller@desactivar')->name('push_devices_desactivar');
    Route::get('push_codes', 'push_devicescontroller@push_codes')->name('push_codes');
   
    

    Route::post('cat_zonas', 'cat_zonas_controller@add')->name('zonas_Add');
    Route::get('cat_zonas', 'cat_zonas_controller@getAll')->name('zonas_All');
    Route::get('cat_zonas/{id}', 'cat_zonas_controller@get')->name('zonas_Get');
    Route::post('cat_zonas/filter', 'cat_zonas_controller@filter')->name('zonas_Filter');
    Route::put('cat_zonas/{id}', 'cat_zonas_controller@edit')->name('zonas_Edit');
    Route::delete('cat_zonas/{id}', 'cat_zonas_controller@delete')->name('zonas_Delete');

    //usuarios-cuent
    Route::post('cat_usuarios', 'cat_usuarios_controller@add')->name('usuarios_Add');
    Route::get('cat_usuarios', 'cat_usuarios_controller@getAll')->name('usuarios_All');
    Route::get('cat_usuarios/{id}', 'cat_usuarios_controller@get')->name('usuarios_Get');
    Route::post('cat_usuarios/filter', 'cat_usuarios_controller@filter')->name('usuarios_Filter');
    Route::put('cat_usuarios/{id}', 'cat_usuarios_controller@edit')->name('usuarios_Edit');
    Route::delete('cat_usuarios/{id}', 'cat_usuarios_controller@delete')->name('usuarios_Delete');


    //busqueda de cuentas ude usuario

    Route::post('filtrarusuario','AuthController@filtrar')->name('buscarusuario');
    Route::get('verificar/{email}','AuthController@verificar')->name('verificar');

    //usuario_cuenta_rel
    Route::post('cuentasusuariorel', 'cuentas_usuarios_rel_controller@add')->name('rel_Add');
    Route::get('cuentasusuariorel/{id}','cuentas_usuarios_rel_controller@agregados')->name('buscar-rel');
    Route::put('cuentasusuariorel/{id}','cuentas_usuarios_rel_controller@edit')->name('editar-rel');

});

//autenticacion de usuario
Route::group(['prefix' => 'auth'], function () {
    Route::post('login', 'AuthController@login');
    
    Route::post('ressetpass', 'resetpass_controller@resetpass');
    //Route::post('send_notifications','send_notificationcontroller@send');
    Route::get('send_notifications','send_notificationcontroller@send');
    Route::get('terminosCondiciones','AuthController@getTerminosCondiciones');

    
  
    Route::group(['middleware' => 'auth:api'], function() {
        Route::get('logout', 'AuthController@logout');
        Route::get('user', 'AuthController@user');
        Route::post('signup', 'AuthController@signup');

    });
});

// administracion de usuarios (seguridad)
Route::group(['prefix' => 'security'], function () {
	
    Route::group(['middleware' => 'auth:api'], function() {
    	// ESTADOS
    	Route::post('estados', 			'Seguridad\cat_estados_controller@add');
    	Route::get('estados', 			'Seguridad\cat_estados_controller@getAll');
    	Route::get('estados/{id}', 		'Seguridad\cat_estados_controller@get');
    	Route::post('estados/filter', 	'Seguridad\cat_estados_controller@filter');
    	Route::put('estados/{id}', 		'Seguridad\cat_estados_controller@edit');
        Route::delete('estados/{id}', 	'Seguridad\cat_estados_controller@delete');
    	
    	// MUNICIPIOS
    	Route::post('municipios', 			'Seguridad\cat_municipios_controller@add');
    	Route::get('municipios', 			'Seguridad\cat_municipios_controller@getAll');
    	Route::get('municipios/{id}', 		'Seguridad\cat_municipios_controller@get');
    	Route::post('municipios/filter', 	'Seguridad\cat_municipios_controller@filter');
    	Route::put('municipios/{id}', 		'Seguridad\cat_municipios_controller@edit');
        Route::delete('municipios/{id}', 	'Seguridad\cat_municipios_controller@delete');

    	// MENU
    	Route::post('cat_menus', 		'Seguridad\cat_menus_controller@add');
        Route::get('cat_menus', 		'Seguridad\cat_menus_controller@getAll');
        Route::get('cat_menus/{id}', 	'Seguridad\cat_menus_controller@get');
        Route::post('cat_menus/filter', 'Seguridad\cat_menus_controller@filter');
        Route::put('cat_menus/{id}', 	'Seguridad\cat_menus_controller@edit');
        Route::delete('cat_menus/{id}', 'Seguridad\cat_menus_controller@delete');

        // PERFILES
    	Route::post('perfiles', 		'Seguridad\perfiles_controller@add');
        Route::get('perfiles', 			'Seguridad\perfiles_controller@getAll');
        Route::get('perfiles/{id}', 	'Seguridad\perfiles_controller@get');
        Route::post('perfiles/filter', 	'Seguridad\perfiles_controller@filter');
        Route::put('perfiles/{id}', 	'Seguridad\perfiles_controller@edit');
        Route::delete('perfiles/{id}', 	'Seguridad\perfiles_controller@delete');

        // PERMISOS
    	Route::post('permisos', 		'Seguridad\permisos_controller@add');
        Route::get('permisos', 			'Seguridad\permisos_controller@getAll');
        Route::get('permisos/{id}', 	'Seguridad\permisos_controller@get');
        Route::post('permisos/filter',	'Seguridad\permisos_controller@filter');
        Route::put('permisos/{id}', 	'Seguridad\permisos_controller@edit');
        Route::delete('permisos/{id}', 	'Seguridad\permisos_controller@delete');
    });
});
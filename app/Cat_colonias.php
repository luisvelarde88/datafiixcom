<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cat_colonias extends Model
{
    protected $connection='fiixcom_soft-central';
    protected $table= 'cat_colonias';
    protected $fillable=array('id_col','key_loc','colonia');   
    public $timestamps = false;
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cat_municipios_master extends Model
{
	public $timestamps = false;
    
    protected $table = 'cat_municipios';

    protected $primaryKey = 'id_mun';

    protected $connection = 'fiixcom_soft-central_master';

	protected $fillable = [
    	'id_mun',
    	'key_edo',
    	"municipio"
    ];
}

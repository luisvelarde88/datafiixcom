<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cat_tecnicos_master extends Model
{
	public $timestamps = false;
    
    protected $table = 'cat_tecnicos';

    protected $connection = 'fiixcom_soft-central_master';

	protected $fillable = [
    	'id',
    	'nombre',
    	'apellidos',
    	'puesto',
    	'estado',
    	'whatsapp',
    	'sms',
    	'llamadas',
    	"extension"
    ];
}

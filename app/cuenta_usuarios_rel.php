<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cuenta_usuarios_rel extends Model
{
	public $timestamps = false;
    
    protected $table = 'cuenta_usuarios_rel';

    protected $connection = 'mysql';

	protected $fillable = [
    	
    	'cuenta',
    	'id_usuario',
    	'timestamp',
    	'status'
    ];
}
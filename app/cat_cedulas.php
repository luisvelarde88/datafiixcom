<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cat_cedulas extends Model
{
	public $timestamps = false;

    protected $table = 'cat_cedulas';

    protected $connection = 'fiixcom_soft-central';

	protected $fillable = [
    	'id',
    	'id_cuenta',
    	"cedula",
    	'hora_cedula',
    	'hora_evento',
    	"margen",
    	'codigo',
    	'hora_notificacion'
    ];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cat_zonas_master extends Model
{
	public $timestamps = false;
    
    protected $table = 'cat_zonas';

    protected $connection = 'fiixcom_soft-central_master';

	protected $fillable = [
    	'id',
    	'id_cuenta',
    	'numero',
    	'descripcion',
    	'timestamp',
    	'estado'
    ];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cat_zonas extends Model
{
	public $timestamps = false;
    
    protected $table = 'cat_zonas';

    protected $connection = 'fiixcom_soft-central';

	protected $fillable = [
    	'id',
    	'id_cuenta',
    	'numero',
    	'descripcion',
    	'timestamp',
    	'estado'
    ];
}

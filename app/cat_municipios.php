<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cat_municipios extends Model
{
	public $timestamps = false;
    
    protected $table = 'cat_municipios';

    protected $primaryKey = 'id_mun';

    protected $connection = 'fiixcom_soft-central';

	protected $fillable = [
    	'id_mun',
    	'key_edo',
    	"municipio"
    ];
}

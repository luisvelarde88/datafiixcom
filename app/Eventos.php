<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Eventos extends Model
{
    protected $connection='fiixcom_soft-central';
    protected $table= 'eventos';
    protected $fillable=array('id','descripcion','codigo','codigo_prefijo ','cuenta','fecha_recibido','fecha_procesado','zona','llamadas','correos','mensajes','notificado','timestamp','raw');   
    public $timestamps = false;
}

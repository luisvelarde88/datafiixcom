<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class push_group_codes extends Model
{
	public $timestamps = false;
    
    protected $table = 'push_group_codes';

    protected $connection = 'mysql';

	protected $fillable = [
    	
    
    	'option_name',
    	'all_codes',
        'codes',
        'status',
        'fecha_update',
        
    ];
}
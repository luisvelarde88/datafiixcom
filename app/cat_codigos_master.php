<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cat_codigos_master extends Model
{
	public $timestamps = false;

	protected $table = 'cat_codigos';

	protected $connection = 'fiixcom_soft-central_master';

    protected $fillable = [
    	'id',
    	'nombre_protocolo',
    	"fecha_add",
    	"estado"
    ];
}

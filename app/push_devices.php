<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class push_devices extends Model
{
	public $timestamps = false;
    
    protected $table = 'push_devices';

    protected $connection = 'mysql';

	protected $fillable = [
    	
    
    	'id_usuario',
    	'token',
        'status',
        'device_type',
        'version',
        'numero_telefonico',
        'fecha_update'
    ];
}
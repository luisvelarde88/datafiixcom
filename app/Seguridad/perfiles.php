<?php

namespace App\Seguridad;

use Illuminate\Database\Eloquent\Model;

class perfiles extends Model
{
	public $timestamps = false;
    
    protected $table = 'perfiles';

    protected $connection = 'mysql';

	protected $fillable = [
    	'id',
    	'nombre',
    	'status',
    	'created_at'
    ];
}

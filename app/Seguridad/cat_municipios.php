<?php

namespace App\Seguridad;

use Illuminate\Database\Eloquent\Model;

class cat_municipios extends Model
{
	public $timestamps = false;
    
    protected $table = 'cat_municipios';

    protected $connection = 'mysql';

    protected $primaryKey = 'id_municipio';

	protected $fillable = [
    	'id_municipio',
    	'nombre_municipio',
    	'estado'
    ];
}

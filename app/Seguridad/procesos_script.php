<?php

namespace App\Seguridad;

use Illuminate\Database\Eloquent\Model;

class procesos_script extends Model
{
	public $timestamps = false;
    
    protected $table = 'procesos_scripts';

    protected $connection = 'mysql';

	protected $fillable = [
    	'id',
    	'tarea',
    	'valor',
    	'status',
    	'log',
    	"created_at",
        "updated_at"
    ];
}

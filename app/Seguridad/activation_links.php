<?php

namespace App\Seguridad;

use Illuminate\Database\Eloquent\Model;

class activation_links extends Model
{
	public $timestamps = false;
    
    protected $table = 'activation_links';

    protected $connection = 'mysql';

	protected $fillable = [
    	'id',
    	'token',
        'idusuario',
        'expiration_at',
        'status',
        'created_at',
        'updated_at'
    ];
}

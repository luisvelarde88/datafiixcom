<?php

namespace App\Seguridad;

use Illuminate\Database\Eloquent\Model;

class cat_menus extends Model
{
	public $timestamps = false;
    
    protected $table = 'cat_menus';

    protected $connection = 'mysql';

	protected $fillable = [
    	'id',
    	'idperfil',
    	'menu',
    	'orden',
    	'status',
    	"created_at"
    ];
}

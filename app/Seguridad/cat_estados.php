<?php

namespace App\Seguridad;

use Illuminate\Database\Eloquent\Model;

class cat_estados extends Model
{
	public $timestamps = false;
    
    protected $table = 'cat_estados';

    protected $connection = 'mysql';

    protected $primaryKey = 'id_estado';

	protected $fillable = [
    	'id_estado',
    	'estado'
    ];
}

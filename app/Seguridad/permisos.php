<?php

namespace App\Seguridad;

use Illuminate\Database\Eloquent\Model;

class permisos extends Model
{
	public $timestamps = false;
    
    protected $table = 'permisos';

    protected $connection = 'mysql';

	protected $fillable = [
    	'id',
    	'modulo_ws',
    	'perfil_permitido',
    	'permiso',
    	'status',
    	'created_at'
    ];
}

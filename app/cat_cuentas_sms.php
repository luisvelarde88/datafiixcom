<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cat_cuentas_sms extends Model
{
    //
    protected $connection='fiixcom_soft-central';
    protected $table= 'cat_cuentas_sms';
    protected $fillable=array('numero_cuenta','sms','registro','fecha','nombre');   
    public $timestamps = false;
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\cuenta_usuarios_rel;
use App\cat_cuentas;


class cuentas_usuarios_rel_controller extends Controller
{
    public function add(Request $request)
    {
        $userrels= $request->json()->all();
      
         foreach($userrels as $userrel){
            $comprobacion=cuenta_usuarios_rel::where([['id_usuario','=',$userrel['id_usuario']],['cuenta','=',$userrel['cuenta']],['status','=',1]])->get();
            if($comprobacion->isEmpty()){
                 $nuevoregistro=cuenta_usuarios_rel::create($userrel);
                 return response()->json([
                    'message' => 'Se guardaron los cambios'], 201);
            }else{
            return response()->json([
                'error' => 'no se guardo'], 201);
            }
         }
    
            
    }

    public function getAll()
    {
        $cuentas_usuarios_rel = cuenta_usuarios_rel::all();

        return $cuentas_usuarios_rel;
    }

    public function get($id)
    {
        $cuentas_usuarios_rel = cuenta_usuarios_rel::find($id);

        return $cuentas_usuarios_rel;
    }

    

    public function filter(Request $request)
    {
        $cuentas_usuarios_rel = new cuenta_usuarios_rel();
        $cuentas_usuarios_rel = $cuentas_usuarios_rel->newQuery();

        $filtros = [
            ['cuenta', 	'=', 	"obligatorio"],
            ['id_usuario', 		'like', ""],
    
            ['status', '=', ""],
            ['timestamp', 	'=', 	""],
        ];

        $opcionales = [];

        foreach ($filtros as $filtro)
        {
            if ($request->has($filtro[0]))
            {
            	if ($filtro[2] == "obligatorio")
            	{
            		if ($filtro[1] == 'like')
                	{
                		$cuentas_usuarios_rel->where($filtro[0], "like", "%" . $request->input($filtro[0]) . "%");
                	}
                	else
                	{
                		$cuentas_usuarios_rel->where($filtro[0], $request->input($filtro[0]));
                	}
                }
                else
                {
                	$opcionales[] = $filtro;
            	}
            }
        }

        if ($opcionales)
        {
        	$cuentas_usuarios_rel->where(function($query) use ($opcionales, $request)
	        {
	        	$primerOpcional = $opcionales[0];

	        	if ($primerOpcional[1] == 'like')
	            {
	            	$query->where($primerOpcional[0], "like", "%" . $request->input($primerOpcional[0]) . "%");
	            }
	            else
	            {
	            	$query->where($primerOpcional[0], $request->input($primerOpcional[0]));
	            }

	            for ($i = 1; $i < count($opcionales); $i++)
	            { 
	            	if ($opcionales[$i][1] == 'like')
	                {
	                	$query->orWhere($opcionales[$i][0], "like", "%" . $request->input($opcionales[$i][0]) . "%");
	                }
	                else
	                {
	                	$query->orWhere($opcionales[$i][0], $request->input($opcionales[$i][0]));
	                }
	            }
	        });
        }   

        return $cuentas_usuarios_rel->get();
    }

    public function edit($id, Request $request)
    {
        $cuentas_usuarios_rel = $this->get($id);
        $cuentas_usuarios_rel->fill($request->all())->save();

        return $cuentas_usuarios_rel;
    }

    public function delete($id)
    {
        $cuentas_usuarios_rel = $this->get($id);
        $cuentas_usuarios_rel->delete();

        return $cuentas_usuarios_rel;
    }


    public function agregados($id){
        $cuentas_rel_user=[];
        $cuentas_usuarios_rel=   cuenta_usuarios_rel::where([['id_usuario','=',$id],['status','=',1],])->get();


        foreach($cuentas_usuarios_rel as $user_rel){
           
           
           $cuenta=  cat_cuentas::where('cuenta','=',$user_rel->cuenta)->first();
          
           if (!$cuenta){
            
           }else{
            $user_rel->nombre=$cuenta->nombre;
            $user_rel->apellidos=$cuenta->apellidos;
            $user_rel->particion=$cuenta->particion;

            array_push($cuentas_rel_user, $user_rel);
           }
                
        }

        
       return  $cuentas_rel_user;

    }
}

<?php

namespace App\Http\Controllers;

use App\User;
use App\Seguridad\procesos_script;
use App\Seguridad\activation_links;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class resetpass_controller extends Controller
{
    public function resetpass(Request $request)
    {
        $request->validate([
            'email'       => 'required|string|email',
            
        ]);  

        $user= User::where([['email','=',$request['email']],['status','=',1]])->first();
        if (isset($user->id)){
       $proceso = new procesos_script([
            'tarea'         => 'RECUPERACION',
            'valor'         => $user->id,
            'status'        => 'PENDIENTE',
            'created_at'    => date('Y-m-d h:i:s')
        ]);

        $proceso->save();

        $fecha = date_create(date('Y-m-d h:i:s'));

        $link = new activation_links([
            'token' => hash('sha256', $user->id . $user->name . $user->apellidos . date('Ymdhis')),
            'idusuario' => $user->id,
            'expiration_at' => date_add($fecha, date_interval_create_from_date_string('1 day')),
            'status' => 1,
            'created_at' => date('Y-m-d h:i:s')
        ]);

        $link->save();

        return response()->json([
            'message' => 'Se enviara un correo a su cuenta'], 201);
            
        }else{

            return response()->json([
                'message' => 'Cuenta invalida'], 201);
        }
            
            
            }

    
}
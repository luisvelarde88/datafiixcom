<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\cat_cuentas_sms_master;
class cat_cuentas_sms_masterController extends Controller
{
    public function getAll(){
        $Cat_cunetas_sms=cat_cuentas_sms_master::all();
        return $Cat_cunetas_sms;
    }
    public function add(Request $request){
        $Cat_cunetas_sms=cat_cuentas_sms_master::create($request->all());
        return $Cat_cunetas_sms;

    }

    public function get($id){

        $Cat_cunetas_sms=cat_cuentas_sms_master::find($id);
        return $Cat_cunetas_sms;
    }
    public function edit($id, Request $request){
        $Cat_cunetas_sms=$this->get($id);
        $Cat_cunetas_sms->fill($request->all())->save();
        return $Cat_cunetas_sms;
    }

    public function delete($id){
        $Cat_cunetas_sms= $this->get($id);
        $Cat_cunetas_sms->delete();
        return $Cat_cunetas_sms;

    }

}

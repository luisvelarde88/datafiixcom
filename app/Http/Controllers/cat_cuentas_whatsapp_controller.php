<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\cat_cuentas_whatsapp;
use App\cat_cuentas_whatsapp_master;

class cat_cuentas_whatsapp_controller extends Controller
{
    public function add(Request $request)
    {
        $cat_cuentas_whatsapp = cat_cuentas_whatsapp_master::create($request->all());

        return $cat_cuentas_whatsapp;
    }

    public function getAll()
    {
        $cat_cuentas_whatsapp = cat_cuentas_whatsapp::all();

        return $cat_cuentas_whatsapp;
    }

    public function get($id)
    {
        $cat_cuentas_whatsapp = cat_cuentas_whatsapp::find($id);

        return $cat_cuentas_whatsapp;
    }

    public function getMaster($id)
    {
        $cat_cuentas_whatsapp = cat_cuentas_whatsapp_master::find($id);

        return $cat_cuentas_whatsapp;
    }

    public function filter(Request $request)
    {
        $cat_cuentas_whatsapp = new cat_cuentas_whatsapp();
        $cat_cuentas_whatsapp = $cat_cuentas_whatsapp->newQuery();

        $filtros = [
            'numero_cuenta',
            "whatsapp",
            'registro',
            'fecha',
            "nombre"
        ];

        foreach ($filtros as $filtro)
        {
            if ($request->has($filtro))
            {
                $cat_cuentas_whatsapp->where($filtro, $request->input($filtro));
            }
        }

        return $cat_cuentas_whatsapp->get();
    }

    public function edit($id, Request $request)
    {
        $cat_cuentas_whatsapp = $this->getMaster($id);
        $cat_cuentas_whatsapp->fill($request->all())->save();

        return $cat_cuentas_whatsapp;
    }

    public function delete($id)
    {
        $cat_cuentas_whatsapp = $this->getMaster($id);
        $cat_cuentas_whatsapp->delete();

        return $cat_cuentas_whatsapp;
    }
}

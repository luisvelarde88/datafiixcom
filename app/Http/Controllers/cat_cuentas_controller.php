<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\cat_cuentas;
use App\cat_cuentas_master;
use App\cuenta_usuarios_rel;
use App\User;

class cat_cuentas_controller extends Controller
{
    public function add(Request $request)
    {
        $cat_cuentas = cat_cuentas_master::create($request->all());

        return $cat_cuentas;
    }

    public function getAll()
    {
        $cat_cuentas = cat_cuentas::all();

        return $cat_cuentas;
    }

    public function get($id)
    {
        $cat_cuentas = cat_cuentas::find($id);

        return $cat_cuentas;
    }
    public function getcuenta($id)
    {
        $cat_cuentas = cat_cuentas::where('cuenta','=',$id)->first();

        return $cat_cuentas;
    }

    public function getMaster($id)
    {
        $cat_cuentas = cat_cuentas_master::find($id);

        return $cat_cuentas;
    }

    public function filter(Request $request)
    {
        $cat_cuentas = new cat_cuentas();
        $cat_cuentas = $cat_cuentas->newQuery();

        $filtros = [
            ["filtro" => 'id_codigo', "operador" => "="],
            ["filtro" => 'cuenta', "operador" => "like"],
            ["filtro" => "nombre", "operador" => "like"],
            ["filtro" => 'apellidos', "operador" => "like"],
            ["filtro" => 'id_idioma', "operador" => "="],
            ["filtro" => "particion", "operador" => "like"],
            ["filtro" => 'id_protocolo', "operador" => "="],
            ["filtro" => 'id_estado', "operador" => "="],
            ["filtro" => "id_municipio", "operador" => "="],
            ["filtro" => 'id_localidad', "operador" => "="],
            ["filtro" => "calle", "operador" => "="],
            ["filtro" => "numero", "operador" => "="],
            ["filtro" => "cpostal", "operador" => "="],
            ["filtro" => "id_colonia", "operador" => "="],
            ["filtro" => "TelefonoFijo1", "operador" => "="],
            ["filtro" => 'TelefonoFijo2', "operador" => "="],
            ["filtro" => 'TelefonoFijo3', "operador" => "="],
            ["filtro" => "TelefonoCel1", "operador" => "="],
            ["filtro" => 'TelefonoCel2', "operador" => "="],
            ["filtro" => "TelefonoCel3", "operador" => "="],
            ["filtro" => "TelefonoWhatsapp1", "operador" => "="],
            ["filtro" => "TelefonoWhatsapp2", "operador" => "="],
            ["filtro" => "TelefonoWhatsapp3", "operador" => "="],
            ["filtro" => "email", "operador" => "="],
            ["filtro" => "timestamp", "operador" => "="],
            ["filtro" => "estado", "operador" => "="],
            ["filtro" => "cedula", "operador" => "="],
            ["filtro" => 'password', "operador" => "="],
            ["filtro" => 'plataforma', "operador" => "="]
        ];

        foreach ($filtros as $filtro)
        {
            if ($request->has($filtro["filtro"]))
            {
                if ($filtro["operador"] == 'like')
                {
                    $valor = "%" . $request->input($filtro["filtro"]) . "%";
                }
                else
                {
                    $valor = $request->input($filtro["filtro"]);
                }

                $cat_cuentas->orWhere($filtro["filtro"], $filtro["operador"], $valor);
            }
        }

        return $cat_cuentas->get();
    }

    public function edit($id, Request $request)
    {
        $cat_cuentas = $this->getMaster($id);
        $cat_cuentas->fill($request->all())->save();

        return $cat_cuentas;
    }

    public function delete($id)
    {
        $cat_cuentas = $this->getMaster($id);
        $cat_cuentas->delete();

        return $cat_cuentas;
    }

    public function filtro(Request $request){
           
        if ($request->input('perfil')==1 || $request->input('perfil')==3 || $request->input('perfil')==4) 
        {
            $cat_cuentas = new cat_cuentas();
            $cat_cuentas = $cat_cuentas->newQuery();
    
            $filtros = [
                ["filtro" => 'id_codigo', "operador" => "="],
                ["filtro" => 'cuenta', "operador" => "like"],
                ["filtro" => "nombre", "operador" => "like"],
                ["filtro" => 'apellidos', "operador" => "like"],
                ["filtro" => 'id_idioma', "operador" => "="],
                ["filtro" => "particion", "operador" => "like"],
                ["filtro" => 'id_protocolo', "operador" => "="],
                ["filtro" => 'id_estado', "operador" => "="],
                ["filtro" => "id_municipio", "operador" => "="],
                ["filtro" => 'id_localidad', "operador" => "="],
                ["filtro" => "calle", "operador" => "="],
                ["filtro" => "numero", "operador" => "="],
                ["filtro" => "cpostal", "operador" => "="],
                ["filtro" => "id_colonia", "operador" => "="],
                ["filtro" => "TelefonoFijo1", "operador" => "="],
                ["filtro" => 'TelefonoFijo2', "operador" => "="],
                ["filtro" => 'TelefonoFijo3', "operador" => "="],
                ["filtro" => "TelefonoCel1", "operador" => "="],
                ["filtro" => 'TelefonoCel2', "operador" => "="],
                ["filtro" => "TelefonoCel3", "operador" => "="],
                ["filtro" => "TelefonoWhatsapp1", "operador" => "="],
                ["filtro" => "TelefonoWhatsapp2", "operador" => "="],
                ["filtro" => "TelefonoWhatsapp3", "operador" => "="],
                ["filtro" => "email", "operador" => "="],
                ["filtro" => "timestamp", "operador" => "="],
                ["filtro" => "estado", "operador" => "="],
                ["filtro" => "cedula", "operador" => "="],
                ["filtro" => 'password', "operador" => "="],
                ["filtro" => 'plataforma', "operador" => "="]
            ];
    
            foreach ($filtros as $filtro)
            {
                if ($request->has($filtro["filtro"]))
                {
                    if ($filtro["operador"] == 'like')
                    {
                        $valor = "%" . $request->input($filtro["filtro"]) . "%";
                    }
                    else
                    {
                        $valor = $request->input($filtro["filtro"]);
                    }
    
                    $cat_cuentas->orWhere($filtro["filtro"], $filtro["operador"], $valor);
                }
            }
    
            return $cat_cuentas->get();

        }
        else
        {

            $usuario=User::where('email','=',$request->input('email'))->first();

            $id_user=$usuario->id;
            $condiciones=[];

            $cuentas_rel=cuenta_usuarios_rel::where([['id_usuario','=',$id_user],['status','=',1],])->get();

            foreach($cuentas_rel as $cuenta){

                $cuentas=$cuenta->cuenta;
                array_push($condiciones,$cuentas);

            }
            $cuentas_deuser=cat_cuentas::whereIn( 'cuenta',$condiciones)->get();

            return $cuentas_deuser;
        }
    }
    public function filtroNot($id,Request $request){
           
       
 
            
             $condiciones=[];
 
             $cuentas_rel=cuenta_usuarios_rel::where([['id_usuario','=',$id],['status','=',1],])->get();
 
             foreach($cuentas_rel as $cuenta){
 
                 $cuentas=$cuenta->cuenta;
                 array_push($condiciones,$cuentas);
 
             }

             $cuentas_deuser = new cat_cuentas();
                $cuentas_deuser = $cuentas_deuser->newQuery();
             
           //  return $cuentas_deuser;
           $filtros = [
            ["filtro" => 'id_codigo', "operador" => "="],
            ["filtro" => 'cuenta', "operador" => "like"],
            ["filtro" => "nombre", "operador" => "like"],
            ["filtro" => 'apellidos', "operador" => "like"],
            ["filtro" => 'id_idioma', "operador" => "="],
            ["filtro" => "particion", "operador" => "like"],
            ["filtro" => 'id_protocolo', "operador" => "="],
            ["filtro" => 'id_estado', "operador" => "="],
            ["filtro" => "id_municipio", "operador" => "="],
            ["filtro" => 'id_localidad', "operador" => "="],
            ["filtro" => "calle", "operador" => "="],
            ["filtro" => "numero", "operador" => "="],
            ["filtro" => "cpostal", "operador" => "="],
            ["filtro" => "id_colonia", "operador" => "="],
            ["filtro" => "TelefonoFijo1", "operador" => "="],
            ["filtro" => 'TelefonoFijo2', "operador" => "="],
            ["filtro" => 'TelefonoFijo3', "operador" => "="],
            ["filtro" => "TelefonoCel1", "operador" => "="],
            ["filtro" => 'TelefonoCel2', "operador" => "="],
            ["filtro" => "TelefonoCel3", "operador" => "="],
            ["filtro" => "TelefonoWhatsapp1", "operador" => "="],
            ["filtro" => "TelefonoWhatsapp2", "operador" => "="],
            ["filtro" => "TelefonoWhatsapp3", "operador" => "="],
            ["filtro" => "email", "operador" => "="],
            ["filtro" => "timestamp", "operador" => "="],
            ["filtro" => "estado", "operador" => "="],
            ["filtro" => "cedula", "operador" => "="],
            ["filtro" => 'password', "operador" => "="],
            ["filtro" => 'plataforma', "operador" => "="]
        ];

        foreach ($filtros as $filtro)
        {
            if ($request->has($filtro["filtro"]))
            {
                if ($filtro["operador"] == 'like')
                {
                    $valor = "%" . $request->input($filtro["filtro"]) . "%";
                }
                else
                {
                    $valor = $request->input($filtro["filtro"]);
                }

                $cuentas_deuser->orWhere($filtro["filtro"], $filtro["operador"], $valor);
            }
        }
        $cuentas_deuser->whereNotIn( 'cuenta',$condiciones);
 
        return $cuentas_deuser->get();
 
        
     }
}

<?php

namespace App\Http\Controllers\Seguridad;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Seguridad\cat_municipios;

class cat_municipios_controller extends Controller
{
    public function add(Request $request)
    {
        $cat_municipios = cat_municipios::create($request->all());

        return $cat_municipios;
    }

    public function getAll()
    {
        $cat_municipios = cat_municipios::all();

        return $cat_municipios;
    }

    public function get($id)
    {
        $cat_municipios = cat_municipios::find($id);

        return $cat_municipios;
    }

    public function filter(Request $request)
    {
        $cat_municipios = new cat_municipios();
        $cat_municipios = $cat_municipios->newQuery();

        $filtros = [
            ['nombre_municipio',    'like', "opcional"],
            ['estado',              '=',    ""]
        ];

        $opcionales = [];

        foreach ($filtros as $filtro)
        {
            if ($request->has($filtro[0]))
            {
                if ($filtro[2] != "opcional")
                {
                    if ($filtro[1] == 'like')
                    {
                        $cat_municipios->where($filtro[0], "like", "%" . $request->input($filtro[0]) . "%");
                    }
                    else
                    {
                        $cat_municipios->where($filtro[0], $request->input($filtro[0]));
                    }
                }
                else
                {
                    $opcionales[] = $filtro;
                }
            }
        }

        if ($opcionales)
        {
            $cat_municipios->where(function($query) use ($opcionales, $request)
            {
                $primerOpcional = $opcionales[0];

                if ($primerOpcional[1] == 'like')
                {
                    $query->where($primerOpcional[0], "like", "%" . $request->input($primerOpcional[0]) . "%");
                }
                else
                {
                    $query->where($primerOpcional[0], $request->input($primerOpcional[0]));
                }

                for ($i = 1; $i < count($opcionales); $i++)
                { 
                    if ($opcionales[$i][1] == 'like')
                    {
                        $query->orWhere($opcionales[$i][0], "like", "%" . $request->input($opcionales[$i][0]) . "%");
                    }
                    else
                    {
                        $query->orWhere($opcionales[$i][0], $request->input($opcionales[$i][0]));
                    }
                }
            });
        }   

        return $cat_municipios->get();
    }

    public function edit($id, Request $request)
    {
        $cat_municipios = $this->get($id);
        $cat_municipios->fill($request->all())->save();

        return $cat_municipios;
    }

    public function delete($id)
    {
        $cat_municipios = $this->get($id);
        $cat_municipios->delete();

        return $cat_municipios;
    }
}

<?php



namespace App\Http\Controllers\Seguridad;



use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Seguridad\cat_menus;



class cat_menus_controller extends Controller

{

    public function add(Request $request)

    {

        $cat_menus = cat_menus::create($request->all());



        return $cat_menus;

    }



    public function getAll()

    {

        $cat_menus = cat_menus::all();



        return $cat_menus;

    }



    public function get($id)

    {

        $cat_menus = cat_menus::find($id);



        return $cat_menus;

    }



    public function filter(Request $request)

    {

        $cat_menus = new cat_menus();

        $cat_menus = $cat_menus->newQuery();



        $filtros = [

            ['idperfil',    'like',    ""],

            ['menu',        'like', "opcional"],

            ['orden',       '=',    ""],

            ['status',      '=',    ""],

            ['created_at',  '=',    ""]

        ];



        $opcionales = [];



        foreach ($filtros as $filtro)

        {

            if ($request->has($filtro[0]))

            {

                if ($filtro[2] != "opcional")

                {

                    if ($filtro[1] == 'like')

                    {

                        $cat_menus->where($filtro[0], "like", "%" . $request->input($filtro[0]) . "%")->orWhere($filtro[0], 0);

                    }

                    else

                    {

                        $cat_menus->where($filtro[0], $request->input($filtro[0]))->orWhere($filtro[0], 0);

                    }

                }

                else

                {

                    $opcionales[] = $filtro;

                }

            }

        }



        if ($opcionales)

        {

            $cat_menus->where(function($query) use ($opcionales, $request)

            {

                $primerOpcional = $opcionales[0];



                if ($primerOpcional[1] == 'like')

                {

                    $query->where($primerOpcional[0], "like", "%" . $request->input($primerOpcional[0]) . "%");

                }

                else

                {

                    $query->where($primerOpcional[0], $request->input($primerOpcional[0]));

                }



                for ($i = 1; $i < count($opcionales); $i++)

                { 

                    if ($opcionales[$i][1] == 'like')

                    {

                        $query->orWhere($opcionales[$i][0], "like", "%" . $request->input($opcionales[$i][0]) . "%");

                    }

                    else

                    {

                        $query->orWhere($opcionales[$i][0], $request->input($opcionales[$i][0]));

                    }

                }

            });

        }   



        return $cat_menus->orderBy('orden', 'asc')->get();

    }



    public function edit($id, Request $request)

    {

        $cat_menus = $this->get($id);

        $cat_menus->fill($request->all())->save();



        return $cat_menus;

    }



    public function delete($id)

    {

        $cat_menus = $this->get($id);

        $cat_menus->delete();



        return $cat_menus;

    }

}


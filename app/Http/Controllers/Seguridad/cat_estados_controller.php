<?php

namespace App\Http\Controllers\Seguridad;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Seguridad\cat_estados;

class cat_estados_controller extends Controller
{
    public function add(Request $request)
    {
        $cat_estados = cat_estados::create($request->all());

        return $cat_estados;
    }

    public function getAll()
    {
        $cat_estados = cat_estados::all();

        return $cat_estados;
    }

    public function get($id)
    {
        $cat_estados = cat_estados::find($id);

        return $cat_estados;
    }

    public function filter(Request $request)
    {
        $cat_estados = new cat_estados();
        $cat_estados = $cat_estados->newQuery();

        $filtros = [
            ['estado',  'like', "opcional"]
        ];

        $opcionales = [];

        foreach ($filtros as $filtro)
        {
            if ($request->has($filtro[0]))
            {
                if ($filtro[2] != "opcional")
                {
                    if ($filtro[1] == 'like')
                    {
                        $cat_estados->where($filtro[0], "like", "%" . $request->input($filtro[0]) . "%");
                    }
                    else
                    {
                        $cat_estados->where($filtro[0], $request->input($filtro[0]));
                    }
                }
                else
                {
                    $opcionales[] = $filtro;
                }
            }
        }

        if ($opcionales)
        {
            $cat_estados->where(function($query) use ($opcionales, $request)
            {
                $primerOpcional = $opcionales[0];

                if ($primerOpcional[1] == 'like')
                {
                    $query->where($primerOpcional[0], "like", "%" . $request->input($primerOpcional[0]) . "%");
                }
                else
                {
                    $query->where($primerOpcional[0], $request->input($primerOpcional[0]));
                }

                for ($i = 1; $i < count($opcionales); $i++)
                { 
                    if ($opcionales[$i][1] == 'like')
                    {
                        $query->orWhere($opcionales[$i][0], "like", "%" . $request->input($opcionales[$i][0]) . "%");
                    }
                    else
                    {
                        $query->orWhere($opcionales[$i][0], $request->input($opcionales[$i][0]));
                    }
                }
            });
        }   

        return $cat_estados->get();
    }

    public function edit($id, Request $request)
    {
        $cat_estados = $this->get($id);
        $cat_estados->fill($request->all())->save();

        return $cat_estados;
    }

    public function delete($id)
    {
        $cat_estados = $this->get($id);
        $cat_estados->delete();

        return $cat_estados;
    }
}

<?php

namespace App\Http\Controllers\Seguridad;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Seguridad\perfiles;

class perfiles_controller extends Controller
{
    public function add(Request $request)
    {
        $perfiles = perfiles::create($request->all());

        return $perfiles;
    }

    public function getAll()
    {
        $perfiles = perfiles::all();

        return $perfiles;
    }

    public function get($id)
    {
        $perfiles = perfiles::find($id);

        return $perfiles;
    }

    public function filter(Request $request)
    {
        $perfiles = new perfiles();
        $perfiles = $perfiles->newQuery();

        $filtros = [
            ['nombre',      'like', "opcional"],
            ['status',      '=',    ""],
            ['created_at',  '=',    ""]
        ];

        $opcionales = [];

        foreach ($filtros as $filtro)
        {
            if ($request->has($filtro[0]))
            {
                if ($filtro[2] != "opcional")
                {
                    if ($filtro[1] == 'like')
                    {
                        $perfiles->where($filtro[0], "like", "%" . $request->input($filtro[0]) . "%");
                    }
                    else
                    {
                        $perfiles->where($filtro[0], $request->input($filtro[0]));
                    }
                }
                else
                {
                    $opcionales[] = $filtro;
                }
            }
        }

        if ($opcionales)
        {
            $perfiles->where(function($query) use ($opcionales, $request)
            {
                $primerOpcional = $opcionales[0];

                if ($primerOpcional[1] == 'like')
                {
                    $query->where($primerOpcional[0], "like", "%" . $request->input($primerOpcional[0]) . "%");
                }
                else
                {
                    $query->where($primerOpcional[0], $request->input($primerOpcional[0]));
                }

                for ($i = 1; $i < count($opcionales); $i++)
                { 
                    if ($opcionales[$i][1] == 'like')
                    {
                        $query->orWhere($opcionales[$i][0], "like", "%" . $request->input($opcionales[$i][0]) . "%");
                    }
                    else
                    {
                        $query->orWhere($opcionales[$i][0], $request->input($opcionales[$i][0]));
                    }
                }
            });
        }   

        return $perfiles->get();
    }

    public function edit($id, Request $request)
    {
        $perfiles = $this->get($id);
        $perfiles->fill($request->all())->save();

        return $perfiles;
    }

    public function delete($id)
    {
        $perfiles = $this->get($id);
        $perfiles->delete();

        return $perfiles;
    }
}

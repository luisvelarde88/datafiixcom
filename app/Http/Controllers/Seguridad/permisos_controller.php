<?php

namespace App\Http\Controllers\Seguridad;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Seguridad\permisos;

class permisos_controller extends Controller
{
    public function add(Request $request)
    {
        $permisos = permisos::create($request->all());

        return $permisos;
    }

    public function getAll()
    {
        $permisos = permisos::all();

        return $permisos;
    }

    public function get($id)
    {
        $permisos = permisos::find($id);

        return $permisos;
    }

    public function filter(Request $request)
    {
        $permisos = new permisos();
        $permisos = $permisos->newQuery();

        $filtros = [
            ['modulo_ws',           'like', ''],
            ['perfil_permitido',    '=',    ''],
            ['permiso',             '=',    ''],
            ['status',              '=',    ''],
            ['created_at',          '=',    '']
        ];

        $opcionales = [];

        foreach ($filtros as $filtro)
        {
            if ($request->has($filtro[0]))
            {
                if ($filtro[2] != "opcional")
                {
                    if ($filtro[1] == 'like')
                    {
                        $permisos->where($filtro[0], "like", "%" . $request->input($filtro[0]) . "%");
                    }
                    else
                    {
                        $permisos->where($filtro[0], $request->input($filtro[0]));
                    }
                }
                else
                {
                    $opcionales[] = $filtro;
                }
            }
        }

        if ($opcionales)
        {
            $permisos->where(function($query) use ($opcionales, $request)
            {
                $primerOpcional = $opcionales[0];

                if ($primerOpcional[1] == 'like')
                {
                    $query->where($primerOpcional[0], "like", "%" . $request->input($primerOpcional[0]) . "%");
                }
                else
                {
                    $query->where($primerOpcional[0], $request->input($primerOpcional[0]));
                }

                for ($i = 1; $i < count($opcionales); $i++)
                { 
                    if ($opcionales[$i][1] == 'like')
                    {
                        $query->orWhere($opcionales[$i][0], "like", "%" . $request->input($opcionales[$i][0]) . "%");
                    }
                    else
                    {
                        $query->orWhere($opcionales[$i][0], $request->input($opcionales[$i][0]));
                    }
                }
            });
        }   

        return $permisos->get();
    }

    public function edit($id, Request $request)
    {
        $permisos = $this->get($id);
        $permisos->fill($request->all())->save();

        return $permisos;
    }

    public function delete($id)
    {
        $permisos = $this->get($id);
        $permisos->delete();

        return $permisos;
    }
}

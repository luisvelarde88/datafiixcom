<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\cat_cedulas;
use App\cat_cedulas_master;

class cat_cedulas_controller extends Controller
{
    public function add(Request $request)
    {
        $cat_cedulas = cat_cedulas_master::create($request->all());

        return $cat_cedulas;
    }

    public function getAll()
    {
        $cat_cedulas = cat_cedulas::all();

        return $cat_cedulas;
    }

    public function get($id)
    {
        $cat_cedulas = cat_cedulas::find($id);

        return $cat_cedulas;
    }

    public function getMaster($id)
    {
        $cat_cedulas = cat_cedulas_master::find($id);

        return $cat_cedulas;
    }

    public function filter(Request $request)
    {
        $cat_cedulas = new cat_cedulas();
        $cat_cedulas = $cat_cedulas->newQuery();

        $filtros = [
            'id_cuenta',
            "cedula",
            'hora_cedula',
            'hora_evento',
            "margen",
            'codigo',
            'hora_notificacion'
        ];

        foreach ($filtros as $filtro)
        {
            if ($request->has($filtro))
            {
                $cat_cedulas->where($filtro, $request->input($filtro));
            }
        }

        return $cat_cedulas->get();
    }

    public function edit($id, Request $request)
    {
        $cat_cedulas = $this->getMaster($id);
        $cat_cedulas->fill($request->all())->save();

        return $cat_cedulas;
    }

    public function delete($id)
    {
        $cat_cedulas = $this->getMaster($id);
        $cat_cedulas->delete();

        return $cat_cedulas;
    }
}

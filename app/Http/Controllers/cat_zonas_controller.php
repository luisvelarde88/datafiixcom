<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\cat_zonas;
use App\cat_zonas_master;

class cat_zonas_controller extends Controller
{
    public function add(Request $request)
    {
        $cat_zonas = cat_zonas_master::create($request->all());

        return $cat_zonas;
    }

    public function getAll()
    {
        $cat_zonas = cat_zonas::all();

        return $cat_zonas;
    }

    public function get($id)
    {
        $cat_zonas = cat_zonas::find($id);

        return $cat_zonas;
    }

    public function getMaster($id)
    {
        $cat_zonas = cat_zonas_master::find($id);

        return $cat_zonas;
    }

    public function filter(Request $request)
    {
        $cat_zonas = new cat_zonas();
        $cat_zonas = $cat_zonas->newQuery();

        $filtros = [
            ['id_cuenta', 	'=', 	"obligatorio"],
            ['numero', 		'like', ""],
            ['descripcion', 'like', ""],
            ['timestamp', 	'=', 	""],
            ['estado', 		'=', 	"obligatorio"]
        ];

        $opcionales = [];

        foreach ($filtros as $filtro)
        {
            if ($request->has($filtro[0]))
            {
            	if ($filtro[2] == "obligatorio")
            	{
            		if ($filtro[1] == 'like')
                	{
                		$cat_zonas->where($filtro[0], "like", "%" . $request->input($filtro[0]) . "%");
                	}
                	else
                	{
                		$cat_zonas->where($filtro[0], $request->input($filtro[0]));
                	}
                }
                else
                {
                	$opcionales[] = $filtro;
            	}
            }
        }

        if ($opcionales)
        {
        	$cat_zonas->where(function($query) use ($opcionales, $request)
	        {
	        	$primerOpcional = $opcionales[0];

	        	if ($primerOpcional[1] == 'like')
	            {
	            	$query->where($primerOpcional[0], "like", "%" . $request->input($primerOpcional[0]) . "%");
	            }
	            else
	            {
	            	$query->where($primerOpcional[0], $request->input($primerOpcional[0]));
	            }

	            for ($i = 1; $i < count($opcionales); $i++)
	            { 
	            	if ($opcionales[$i][1] == 'like')
	                {
	                	$query->orWhere($opcionales[$i][0], "like", "%" . $request->input($opcionales[$i][0]) . "%");
	                }
	                else
	                {
	                	$query->orWhere($opcionales[$i][0], $request->input($opcionales[$i][0]));
	                }
	            }
	        });
        }   

        return $cat_zonas->get();
    }

    public function edit($id, Request $request)
    {
        $cat_zonas = $this->getMaster($id);
        $cat_zonas->fill($request->all())->save();

        return $cat_zonas;
    }

    public function delete($id)
    {
        $cat_zonas = $this->getMaster($id);
        $cat_zonas->delete();

        return $cat_zonas;
    }
}

<?php

namespace App\Http\Controllers;

use App\User;
use App\Configuracion;
use App\Seguridad\procesos_script;
use App\Seguridad\activation_links;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class AuthController extends Controller
{
    public function signup(Request $request)
    {
        $request->validate([
            'name'          => 'required|string',
            'apellidos'     => 'required|string',
            'email'         => 'required|string|email|unique:users',
            'password'      => 'required|string',
            'direccion'     => 'required|string',
            'idciudad'      => 'required|integer',
            'idestado'      => 'required|integer',
            'celular'       => 'required|string',
            'idperfil'      => 'required|integer',
            'created_by'    => 'required|integer'
        ]);

        $user = new User([
            'name'          => $request->name,
            'apellidos'     => $request->apellidos,
            'email'         => $request->email,
            'password'      => bcrypt($request->password),
            'direccion'     => $request->direccion,
            'idciudad'      => $request->idciudad,
            'idestado'      => $request->idestado,
            'celular'       => $request->celular,
            'idperfil'      => $request->idperfil,
            'created_by'    => $request->created_by,
            'status'        => 1
        ]);
        
        $user->save();

        $proceso = new procesos_script([
            'tarea'         => 'ACTIVACION',
            'valor'         => $user->id,
            'status'        => 'PENDIENTE',
            'created_at'    => date('Y-m-d h:i:s')
        ]);

        $proceso->save();

        $fecha = date_create(date('Y-m-d h:i:s'));

        $link = new activation_links([
            'token' => hash('sha256', $user->id . $user->name . $user->apellidos . date('Ymdhis')),
            'idusuario' => $user->id,
            'expiration_at' => date_add($fecha, date_interval_create_from_date_string('1 day')),
            'status' => 1,
            'created_at' => date('Y-m-d h:i:s')
        ]);

        $link->save();

        return response()->json([
            'message' => 'Usuario creado correctamente!'], 201);
    }

    public function login(Request $request)
    {
        $request->validate([
            'email'       => 'required|string|email',
            'password'    => 'required|string',
            'remember_me' => 'boolean',
        ]);        

        $credentials = request(['email', 'password']);
        
        if (!Auth::attempt($credentials)) 
        {
            return response()->json([
                'message' => 'Unauthorized'], 401);
        }        

        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;

        if ($request->remember_me) 
        {
            $token->expires_at = Carbon::now()->addYear();
        }        

        $token->save();  
        $usuario= User::where('email','=',$request['email'])->first();       

        return response()->json([
            'access_token'  => $tokenResult->accessToken,
            'token_type'    => 'Bearer',
            'expires_at'    => Carbon::parse($tokenResult->token->expires_at)->toDateTimeString(),
            'idperfil'      => $this->filter($request)[0]->idperfil,
            'id'            => $this->filter($request)[0]->id
        ]);
    }

    public function logout(Request $request)
    {
        $request->user()->token()->revoke();       

        return response()->json(['message' => 
            'Successfully logged out']);
    }

    public function user(Request $request)
    {
        return response()->json($request->user());
    }

    public function filter(Request $request)
    {
        $users = new User();
        $users = $users->newQuery();

        $filtros = [
            ['email', '=', '']
        ];

        foreach ($filtros as $filtro)
        {
            if ($request->has($filtro[0]))
            {
                $users->where($filtro[0], $request->input($filtro[0]));
            }
        }

        return $users->get();
    }


    public function filtrar(Request $request)
    {
        $user = new User();
        $user = $user->newQuery();

        $filtros = [
            ["filtro" => 'id', "operador" => "="],
            ["filtro" => 'name', "operador" => "like"],
            ["filtro" => "apellidos", "operador" => "like"],
            ["filtro" => 'email', "operador" => "like"],
           
            
        ];

        foreach ($filtros as $filtro)
        {
            if ($request->has($filtro["filtro"]))
            {
                if ($filtro["operador"] == 'like')
                {
                    $valor = "%" . $request->input($filtro["filtro"]) . "%";
                }
                else
                {
                    $valor = $request->input($filtro["filtro"]);
                }

                $user->orWhere($filtro["filtro"], $filtro["operador"], $valor);
            }
        }

        return $user->get();
    }

    public function verificar($email){

        $perfil=User::where('email','=',$email)->first();

        return $perfil;



    }

    public function getTerminosCondiciones() {
        $result = array("status" => false, "result" => "");

        try
        {
            $data_terminosCondiciones = Configuracion::where("nombre", '=', "terminosCondiciones")->first();

            $result["result"] = empty($data_terminosCondiciones->valor) ? "" : $data_terminosCondiciones->valor;
            $result["status"] = true;
        }
        catch (\Throwable $th)
        {
            $result["result"] = $th->getMessage();
        }

        return $result;
    }


}

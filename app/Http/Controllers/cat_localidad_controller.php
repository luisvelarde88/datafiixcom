<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\cat_localidad;
use App\cat_localidad_master;

class cat_localidad_controller extends Controller
{
    public function add(Request $request)
    {
        $cat_localidad = cat_localidad_master::create($request->all());

        return $cat_localidad;
    }

    public function getAll()
    {
        $cat_localidad = cat_localidad::all();

        return $cat_localidad;
    }

    public function get($id)
    {
        $cat_localidad = cat_localidad::find($id);

        return $cat_localidad;
    }

    public function getMaster($id)
    {
        $cat_localidad = cat_localidad_master::find($id);

        return $cat_localidad;
    }

    public function filter(Request $request)
    {
        $cat_localidad = new cat_localidad();
        $cat_localidad = $cat_localidad->newQuery();

        $filtros = [
            'key_mun',
            "localidad"
        ];

        foreach ($filtros as $filtro)
        {
            if ($request->has($filtro))
            {
                $cat_localidad->where($filtro, $request->input($filtro));
            }
        }

        return $cat_localidad->get();
    }

    public function edit($id, Request $request)
    {
        $cat_localidad = $this->getMaster($id);
        $cat_localidad->fill($request->all())->save();

        return $cat_localidad;
    }

    public function delete($id)
    {
        $cat_localidad = $this->getMaster($id);
        $cat_localidad->delete();

        return $cat_localidad;
    }
}

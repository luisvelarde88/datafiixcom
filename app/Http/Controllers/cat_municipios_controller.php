<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\cat_municipios;
use App\cat_municipios_master;

class cat_municipios_controller extends Controller
{
    public function add(Request $request)
    {
        $cat_municipios = cat_municipios_master::create($request->all());

        return $cat_municipios;
    }

    public function getAll()
    {
        $cat_municipios = cat_municipios::all();

        return $cat_municipios;
    }

    public function get($id)
    {
        $cat_municipios = cat_municipios::find($id);

        return $cat_municipios;
    }

    public function getMaster($id)
    {
        $cat_municipios = cat_municipios_master::find($id);

        return $cat_municipios;
    }

    public function filter(Request $request)
    {
        $cat_municipios = new cat_municipios();
        $cat_municipios = $cat_municipios->newQuery();

        $filtros = [
            'key_edo',
            "municipio"
        ];

        foreach ($filtros as $filtro)
        {
            if ($request->has($filtro))
            {
                $cat_municipios->where($filtro, $request->input($filtro));
            }
        }

        return $cat_municipios->get();
    }

    public function edit($id, Request $request)
    {
        $cat_municipios = $this->getMaster($id);
        $cat_municipios->fill($request->all())->save();

        return $cat_municipios;
    }

    public function delete($id)
    {
        $cat_municipios = $this->getMaster($id);
        $cat_municipios->delete();

        return $cat_municipios;
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\cat_cuentas_sms;
use App\cat_cuentas_sms_master;

class cat_cuentas_smsController extends Controller
{
    public function getAll(){
        $Cat_cunetas_sms=cat_cuentas_sms::all();
        return $Cat_cunetas_sms;
    }
    public function add(Request $request){
        $Cat_cunetas_sms=cat_cuentas_sms_master::create($request->all());
        return $Cat_cunetas_sms;

    }

    public function get($id){

        $Cat_cunetas_sms=cat_cuentas_sms::find($id);
        return $Cat_cunetas_sms;
    }

    public function get2($id){

        $Cat_cunetas_sms=cat_cuentas_sms_master::find($id);
        return $Cat_cunetas_sms;
    }
    public function edit($id, Request $request){
        $Cat_cunetas_sms=$this->get2($id);
        $Cat_cunetas_sms->fill($request->all())->save();
        return $Cat_cunetas_sms;
    }

    public function delete($id){
        $Cat_cunetas_sms= $this->get2($id);
        $Cat_cunetas_sms->delete();
        return $Cat_cunetas_sms;

    }
   
    public function getfil(Request $request)
    {    
        $cuentas = new cat_cuentas_sms();
        $cuentas = $cuentas->newQuery();

        $filtros = [
            ['numero_cuenta',   '=',    "obligatorio"],
            ['sms',             'like', ""],
            ['nombre',          'like', ""],
            ['fecha',           '=',    ""],
            ['registro',        '=',    "obligatorio"]
        ];

        $opcionales = [];

        foreach ($filtros as $filtro)
        {
            if ($request->has($filtro[0]))
            {
                if ($filtro[2] == "obligatorio")
                {
                    if ($filtro[1] == 'like')
                    {
                        $cuentas->where($filtro[0], "like", "%" . $request->input($filtro[0]) . "%");
                    }
                    else
                    {
                        $cuentas->where($filtro[0], $request->input($filtro[0]));
                    }
                }
                else
                {
                    $opcionales[] = $filtro;
                }
            }
        }

        if ($opcionales)
        {
            $cuentas->where(function($query) use ($opcionales, $request)
            {
                $primerOpcional = $opcionales[0];

                if ($primerOpcional[1] == 'like')
                {
                    $query->where($primerOpcional[0], "like", "%" . $request->input($primerOpcional[0]) . "%");
                }
                else
                {
                    $query->where($primerOpcional[0], $request->input($primerOpcional[0]));
                }

                for ($i = 1; $i < count($opcionales); $i++)
                { 
                    if ($opcionales[$i][1] == 'like')
                    {
                        $query->orWhere($opcionales[$i][0], "like", "%" . $request->input($opcionales[$i][0]) . "%");
                    }
                    else
                    {
                        $query->orWhere($opcionales[$i][0], $request->input($opcionales[$i][0]));
                    }
                }
            });
        }   

        return $cuentas->get();
    }

}

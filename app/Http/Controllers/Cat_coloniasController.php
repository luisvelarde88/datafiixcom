<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cat_colonias;
class Cat_coloniasController extends Controller
{
    public function getAll(){
        $cat_colonias=Cat_colonias::all();
        return $cat_colonias;
    }

    public function add(Request $request){
        $cat_colonias=Cat_colonias_master::create($request->all());
        return $cat_colonias;

    }

    public function get($id){

        $cat_colonias=Cat_colonias::find($id);
        return $cat_colonias;
    }

    public function get2($id){

        $cat_colonias=Cat_colonias_master::find($id);
        return $cat_colonias;
    }
    public function edit($id, Request $request){
        $cat_colonias=$this->get2($id);
        $cat_colonias->fill($request->all())->save();
        return $cat_colonias;
    }

    public function delete($id){
        $cat_colonias= $this->get2($id);
        $cat_colonias->delete();
        return $cat_colonias;

    }
  
  
  
    public function getfil(Request $request){

        $cat_colonias=Cat_colonias::where('id_col','!=','Null');

        if ($request['id_col'] != ''){
            $cat_colonias ->where ('id_col','=',$request['id_col']);
        }

        if ($request['key_loc'] != ''){
            $cat_colonias ->where ('key_loc','=',$request['key_loc']);
        
        }

        if ($request['colonia'] != ''){
            $cat_colonias ->where ('colonia','=',$request['colonia']);
        }

        return $cat_colonias ->get();


    }



}

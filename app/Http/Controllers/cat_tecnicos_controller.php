<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\cat_tecnicos;
use App\cat_tecnicos_master;

class cat_tecnicos_controller extends Controller
{
    public function add(Request $request)
    {
        $cat_tecnicos = cat_tecnicos_master::create($request->all());

        return $cat_tecnicos;
    }

    public function getAll()
    {
        $cat_tecnicos = cat_tecnicos::all();

        return $cat_tecnicos;
    }

    public function get($id)
    {
        $cat_tecnicos = cat_tecnicos::find($id);

        return $cat_tecnicos;
    }

    public function getMaster($id)
    {
        $cat_tecnicos = cat_tecnicos_master::find($id);

        return $cat_tecnicos;
    }

    public function filter(Request $request)
    {
        $cat_tecnicos = new cat_tecnicos();
        $cat_tecnicos = $cat_tecnicos->newQuery();

        $filtros = [
            'nombre',
            'apellidos',
            'puesto',
            'estado',
            'whatsapp',
            'sms',
            'llamadas',
            "extension"
        ];

        foreach ($filtros as $filtro)
        {
            if ($request->has($filtro))
            {
                $cat_tecnicos->where($filtro, $request->input($filtro));
            }
        }

        return $cat_tecnicos->get();
    }

    public function edit($id, Request $request)
    {
        $cat_tecnicos = $this->getMaster($id);
        $cat_tecnicos->fill($request->all())->save();

        return $cat_tecnicos;
    }

    public function delete($id)
    {
        $cat_tecnicos = $this->getMaster($id);
        $cat_tecnicos->delete();

        return $cat_tecnicos;
    }
}

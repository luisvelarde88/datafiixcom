<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\cat_codigos_detalles;
use App\cat_codigos_detalles_master;

class cat_codigos_detalles_controller extends Controller
{
    public function add(Request $request)
    {
        $cat_codigos_detalles = cat_codigos_detalles_master::create($request->all());

        return $cat_codigos_detalles;
    }

    public function getAll()
    {
        $cat_codigos_detalles = cat_codigos_detalles::all();

        return $cat_codigos_detalles;
    }

    public function get($id)
    {
        $cat_codigos_detalles = cat_codigos_detalles::find($id);

        return $cat_codigos_detalles;
    }

    public function getMaster($id)
    {
        $cat_codigos_detalles = cat_codigos_detalles_master::find($id);

        return $cat_codigos_detalles;
    }

    public function filter(Request $request)
    {
        $cat_codigos_detalles = new cat_codigos_detalles();
        $cat_codigos_detalles = $cat_codigos_detalles->newQuery();

        $filtros = [
            'id_codigo',
            "codigo",
            'nombre',
            'descripcion',
            "prioridad",
            'caracteres_zona',
            'estado',
            "timestamp",
            'restore'
        ];

        foreach ($filtros as $filtro)
        {
            if ($request->has($filtro))
            {
                $cat_codigos_detalles->where($filtro, $request->input($filtro));
            }
        }

        return $cat_codigos_detalles->get();
    }

    public function edit($id, Request $request)
    {
        $cat_codigos_detalles = $this->getMaster($id);
        $cat_codigos_detalles->fill($request->all())->save();

        return $cat_codigos_detalles;
    }

    public function delete($id)
    {
        $cat_codigos_detalles = $this->getMaster($id);
        $cat_codigos_detalles->delete();

        return $cat_codigos_detalles;
    }
}

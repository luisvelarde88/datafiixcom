<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Eventos;
use App\Eventos_master;
use App\cat_codigos_detalles;
use App\cuenta_usuarios_rel;
use App\User;
use App\cat_cuentas;
use App\cat_zonas;
use App\cat_usuarios;
use App\push_group_codes;
use App\view_eventos_ws;


class EventosController extends Controller
{
    public function getAll(){
        $eventos=Eventos::all();
        return $eventos;
    }
    public function add(Request $request){
        $eventos=Eventos::create($request->all());
        return $eventos;

    }

    public function get($id){
        $result = array();

        $data_eventos = Eventos::Join('cat_cuentas','eventos.cuenta','=','cat_cuentas.cuenta')
        ->leftJoin('cat_zonas', function($join) {
            $join->on('cat_zonas.id_cuenta', '=', 'eventos.cuenta')->whereRaw("CAST(cat_zonas.numero AS INT) = CAST(eventos.zona AS INT)");
        })
        ->Join('cat_codigos_detalles','eventos.codigo','=','cat_codigos_detalles.codigo')->select('eventos.*','cat_cuentas.particion','cat_cuentas.nombre', 'cat_cuentas.calle','cat_cuentas.Telefonofijo1','cat_cuentas.Telefonofijo2','cat_cuentas.Telefonofijo3','cat_cuentas.TelefonoCel1','cat_cuentas.TelefonoCel2','cat_cuentas.TelefonoCel3','cat_cuentas.TelefonoWhatsapp1','cat_cuentas.TelefonoWhatsapp2','cat_cuentas.TelefonoWhatsapp3','cat_cuentas.apellidos','cat_zonas.descripcion as origen','cat_codigos_detalles.prioridad');
        $data_eventos->where('eventos.id','=',$id);
        $result = $data_eventos->first();

        $data_codes = push_group_codes::where("codes", "LIKE", "%".$result->codigo."%")->first();

        if(empty($data_codes->valor_campo_zona) || $data_codes->valor_campo_zona == "DISPOSITIVO_ID")
        {
            $result->origen = $result->zona .' '. $result->origen;
            $result->valor_campo_zona = "DISPOSITIVO_ID";
        }
        else
        {
            $data_usuario = cat_usuarios::whereRaw("id_cuenta = '".$result->cuenta."' AND CAST(usuario AS INT) = '".(int)$result->zona."' AND estado = 'ACTIVO'")->first();
            $result->origen = $result->zona .' '. (empty($data_usuario->nombre_usuario)? "" : $data_usuario->nombre_usuario);
            $result->valor_campo_zona = "USUARIO_ID";
        }
        return $result;
    }

    public function getdata($id){


        $eventos=Eventos::find($id);
        $zona=cat_zonas::where('id_cuenta','=',$eventos->cuenta)->first();
        $cuenta=cat_cuentas::where('cuenta','=',$eventos->cuenta)->first();
       if($zona){
        $eventos->zona_desc=$zona->descripcion;
       }
       if($cuenta){ 
       $eventos->particion=$cuenta->particion;
       }

        return $eventos;

    }

    public function getCuenta($cuenta,$codigo){

       
       $eventos=Eventos::where ('cuenta','=',$cuenta)->where('codigo','=',$codigo)->get();
       
       return $eventos;
    }

    public function getCuentafecha($cuenta,$fecha_inicio, $fecha_fin){

    
        $eventos=Eventos::where ('cuenta','=',$cuenta)->whereBetween('fecha_recibido',[$fecha_inicio,$fecha_fin])->get();
        
        return $eventos;
     }
     


    public function edit($id, Request $request){
        $eventos=$this->get($id);
        $eventos->fill($request->all())->save();
        return $eventos;
    }

    public function delete($id){
        $eventos= $this->get($id);
        $eventos->delete();
        return $eventos;

    }

    public function getCuentafil(Request $request){
        $eventos=Eventos::where('id','!=','Null');
        if ($request['cuenta'] != ''){
            $eventos ->where ('cuenta','=',$request['cuenta']);
        
        
        }
        if ($request['codigo'] !=''){
            $eventos->where('codigo','=',$request['codigo']);
        }

        if ($request['zona'] !=''){

            $eventos->where('zona','=',$request['zona']);
        }
       
        if ($request['llamadas'] !=''){
            $eventos->where('llamadas','like',$request['llamadas']);
        }

        if($request['correos'] !=''){
            $eventos->where('correos','like',$request['correos']);
        }

        if ($request['mensajes'] !=''){
            $eventos->where('mensajes','like',$request['mensajes']);
        }

        if ($request['notoficado'] !=''){
            $eventos->where('notificado','like',$request['notoficado']);
        }

        if($request['cuenta'] !=''){

            $eventos->where('cuenta','>=',$request['cuenta']);
        }
       
       
        if($request['fechainicio'] !=''){

            $eventos->where('fecha_recibido','>=',$request['fechainicio']);
        }

        if($request['fechafin'] !=''){

            $eventos->where('fecha_recibido','<=',$request['fechafin']);
        }
       
        return $eventos ->get();
        
        
        
     }


     public function Cuentafil(Request $request){
        if ($request->input('perfil')==0){

            $usuario=User::where('email','=',$request->input('email'))->first();
            
            $id_user=$usuario->id;
            $condiciones=[];

            $cuentas_rel=cuenta_usuarios_rel::where([['id_usuario','=',$id_user],['status','=',1],])->get();
            
            foreach($cuentas_rel as $cuenta){

                $cuentas=$cuenta->cuenta;
                array_push($condiciones,$cuentas);

            }
       
        $eventos=Eventos::whereIn( 'cuenta',$condiciones);
        
       
        if ($request['codigo'] !=''){
            $eventos->where('codigo','=',$request['codigo']);
        }

        if ($request['zona'] !=''){

            $eventos->where('zona','=',$request['zona']);
        }
       
        if ($request['llamadas'] !=''){
            $eventos->where('llamadas','like',$request['llamadas']);
        }

        if($request['correos'] !=''){
            $eventos->where('correos','like',$request['correos']);
        }

        if ($request['mensajes'] !=''){
            $eventos->where('mensajes','like',$request['mensajes']);
        }

        if ($request['notoficado'] !=''){
            $eventos->where('notificado','like',$request['notoficado']);
        }

        if($request['cuenta'] !=''){

            $eventos->where('cuenta','>=',$request['cuenta']);
        }
       
       
        if($request['fechainicio'] !=''){

            $eventos->where('fecha_recibido','>=',$request['fechainicio']);
        }

        if($request['fechafin'] !=''){

            $eventos->where('fecha_recibido','<=',$request['fechafin']);
        }
       
        $eventos->orderBy('fecha_recibido','DESC');
        $eventos->take(100);
      
       //$enven=Eventos::paginate(20);
       
       return response()->json($eventos->paginate(20), 200) ;
       
    }
        
        
     }
     public function Cuentafil2(Request $request){
      
        if ($request->input('perfil')==0){

            $usuario=User::where('email','=',$request->input('email'))->first();
            
            $id_user=$usuario->id;
            $condiciones=[];

            $cuentas_rel=cuenta_usuarios_rel::where([['id_usuario','=',$id_user],['status','=',1],])->get();
            
            foreach($cuentas_rel as $cuenta){

                $cuentas=$cuenta->cuenta;
                array_push($condiciones,$cuentas);

            }
       
        $eventos=view_eventos_ws::whereIn( 'cuenta',$condiciones);

        
       
        if ($request['codigo'] !=''){
            $eventos->where('codigo','=',$request['codigo']);
        }

        if ($request['zona'] !=''){

            $eventos->where('zona','=',$request['zona']);
        }
       
        if ($request['llamadas'] !=''){
            $eventos->where('llamadas','like',$request['llamadas']);
        }

        if($request['correos'] !=''){
            $eventos->where('correos','like',$request['correos']);
        }

        if ($request['mensajes'] !=''){
            $eventos->where('mensajes','like',$request['mensajes']);
        }

        if ($request['notoficado'] !=''){
            $eventos->where('notificado','like',$request['notoficado']);
        }

        if($request['cuenta'] !=''){

            $eventos->where('cuenta','>=',$request['cuenta']);
        }
       
       
        if($request['fechainicio'] !=''){

            $eventos->where('fecha_recibido','>=',$request['fechainicio']);
        }

        if($request['fechafin'] !=''){

            $eventos->where('fecha_recibido','<=',$request['fechafin']);
        }
       
        $eventos->limit(100);
        $eventos->orderBy('fecha_recibido','DESC');
        
       //return $eventos->getQuery()->toSql();
       //$enven=Eventos::paginate(20);
       
       return response()->json($eventos->paginate(20)->onEachSide(5), 200) ;
       
    }
        
        
     }
     public function Cuentafil3($offset,Request $request){
      
        if ($request->input('perfil')==0){

            $usuario=User::where('email','=',$request->input('email'))->first();
            
            $id_user=$usuario->id;
            $condiciones=[];

            $cuentas_rel=cuenta_usuarios_rel::where([['id_usuario','=',$id_user],['status','=',1],])->get();
            
            foreach($cuentas_rel as $cuenta){

                $cuentas=$cuenta->cuenta;
                array_push($condiciones,$cuentas);

            }
       
        $eventos=view_eventos_ws::whereIn( 'cuenta',$condiciones);

        
       
        if ($request['codigo'] !=''){
            $eventos->where('codigo','=',$request['codigo']);
        }

        if ($request['zona'] !=''){

            $eventos->where('zona','=',$request['zona']);
        }
       
        if ($request['llamadas'] !=''){
            $eventos->where('llamadas','like',$request['llamadas']);
        }

        if($request['correos'] !=''){
            $eventos->where('correos','like',$request['correos']);
        }

        if ($request['mensajes'] !=''){
            $eventos->where('mensajes','like',$request['mensajes']);
        }

        if ($request['notoficado'] !=''){
            $eventos->where('notificado','like',$request['notoficado']);
        }

        if($request['cuenta'] !=''){

            $eventos->where('cuenta','>=',$request['cuenta']);
        }
       
       
        if($request['fechaInicio'] !=''){

            $eventos->where('fecha_recibido','>=',$request['fechaInicio']);
        }

        if($request['fechaFin'] !=''){

            $eventos->where('fecha_recibido','<=',$request['fechaFin']);
        }
        if(!(empty($request['cuentas']))){

            $eventos->whereIn('cuenta',$request['cuentas']);
        }
       
        $eventos->take(20);
        $eventos->orderBy('id','DESC');
        $eventos->skip($offset);
       //return $eventos->getQuery()->toSql();
       //$enven=Eventos::paginate(20);
       
       return $eventos->get();
       
    }elseif($request->input('perfil')==1){

        $eventos=view_eventos_ws::where( 'id','!=','Null');

        
       
        if ($request['codigo'] !=''){
            $eventos->where('codigo','=',$request['codigo']);
        }

        if ($request['zona'] !=''){

            $eventos->where('zona','=',$request['zona']);
        }
       
        if ($request['llamadas'] !=''){
            $eventos->where('llamadas','like',$request['llamadas']);
        }

        if($request['correos'] !=''){
            $eventos->where('correos','like',$request['correos']);
        }

        if ($request['mensajes'] !=''){
            $eventos->where('mensajes','like',$request['mensajes']);
        }

        if ($request['notoficado'] !=''){
            $eventos->where('notificado','like',$request['notoficado']);
        }

        if($request['cuenta'] !=''){

            $eventos->where('cuenta','>=',$request['cuenta']);
        }
       
       
        if($request['fechaInicio'] !=''){

            $eventos->where('fecha_recibido','>=',$request['fechaInicio']);
        }

        if($request['fechaFin'] !=''){

            $eventos->where('fecha_recibido','<=',$request['fechaFin']);
        }

        if(!(empty($request['cuentas']))){

            $eventos->whereIn('cuenta',$request['cuentas']);
        }
       
        $eventos->take(20);
        $eventos->orderBy('id','DESC');
        $eventos->skip($offset);
       //return $eventos->getQuery()->toSql();
       //$enven=Eventos::paginate(20);
       
       return $eventos->get();



    }
        
        
     }
    public function panico(Request $request){


        $codigo=cat_codigos_detalles::where('id','=',349)->first();


        $evento = new Eventos_master;
        $evento->descripcion=$codigo->descripcion;
        $evento->codigo=$codigo->codigo;
        $evento->cuenta=$request->cuenta;
        $evento->codigo_prefijo="E120";
        $evento->fecha_recibido=$request->fecha;
        $evento->zona="00";
        $evento->llamadas="PENDIENTE";
        $evento->correos="PENDIENTE";
        $evento->mensajes="PENDIENTE";
        $evento->notificacion="SI";
        $evento->timestamp=$request->fecha;
        $evento->raw="";
        $evento->save();

        return $evento;


    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\cat_estados;
use App\cat_estados_master;

class cat_estados_controller extends Controller
{
    public function add(Request $request)
    {
        $cat_estados = cat_estados_master::create($request->all());

        return $cat_estados;
    }

    public function getAll()
    {
        $cat_estados = cat_estados::all();

        return $cat_estados;
    }

    public function get($id)
    {
        $cat_estados = cat_estados::find($id);

        return $cat_estados;
    }

    public function getMaster($id)
    {
        $cat_estados = cat_estados_master::find($id);

        return $cat_estados;
    }

    public function filter(Request $request)
    {
        $cat_estados = new cat_estados();
        $cat_estados = $cat_estados->newQuery();

        $filtros = [
            'estado'
        ];

        foreach ($filtros as $filtro)
        {
            if ($request->has($filtro))
            {
                $cat_estados->where($filtro, $request->input($filtro));
            }
        }

        return $cat_estados->get();
    }

    public function edit($id, Request $request)
    {
        $cat_estados = $this->getMaster($id);
        $cat_estados->fill($request->all())->save();

        return $cat_estados;
    }

    public function delete($id)
    {
        $cat_estados = $this->getMaster($id);
        $cat_estados->delete();

        return $cat_estados;
    }
}

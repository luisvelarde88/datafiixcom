<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Eventos;
use App\cuenta_usuarios_rel;
use App\User;
use App\push_devices;
use App\push_allowed_group;
use App\push_group_codes;

class Send_notificationcontroller extends Controller
{
    public function send(Request $request)
    {
        $idsUsuarios = [];

        if( isset($request['descripcion']) )
        {
            $request['descripcion'] = str_replace('_', '%20', $request['descripcion']);
        }

        $users_monitorista = User::select('users.id')
        ->join('perfiles', 'perfiles.id', '=', 'users.idperfil')
        ->where([["users.status", "=", "1"], ["perfiles.status", "=", "1"], ["perfiles.nombre", "=", "monitorista"]])
        ->get();

        foreach ($users_monitorista as $monitorista) 
        {
            array_push($idsUsuarios, $monitorista->id);
        }

        $ususariodecuenta = cuenta_usuarios_rel::where([['cuenta','=',$request['cuenta']],['status','=','1']])->get();

        foreach ($ususariodecuenta as $user) 
        {
            array_push($idsUsuarios, $user->id_usuario);
        }

        $idsUsuarios = array_unique($idsUsuarios);

        if( ! empty($idsUsuarios) )
        {
            foreach($idsUsuarios as $id_usuario)
            {
                $dispositivos = push_devices::select('id', 'device_type', 'token')->where([['id_usuario','=',$id_usuario],['status','=','ACTIVO']])->get();

                //$dispositivos=push_devices::where('id_usuario','=',$user->id_usuario)->get();
                if(!$dispositivos->isEmpty())
                {
                    foreach($dispositivos as $dispositivo)
                    {
                        $grupo = push_allowed_group::where([['id_device','=',$dispositivo->id],['status','=','ACTIVO']])->first();

                        $data_ids_group = explode(',', $grupo->ids_push_group);

                        foreach ($data_ids_group as $id_push_group)
                        {
                            $codigos = push_group_codes::where([['id','=',$id_push_group],['status','=','ACTIVO']])->first();

                            if($codigos->all_codes == 'yes')
                            {
                                $mensaje = $this->requestByCurl('https://data.fiixcom.mx/ws/push/send_notification/?auth_key=ul9DXPmiTma8ECGLU0J35V2JG&device_type='.$dispositivo->device_type.'&device_token='.$dispositivo->token.'&message='.$request['descripcion']);
                                //cdigo para enviar el la informacion
                            }
                            else
                            {
                                $vercodigo = push_group_codes::where('id','=',$id_push_group)->whereRaw('FIND_IN_SET('.$request['codigo'].',codes)');

                                if($vercodigo->first())
                                {
                                    $mensaje = $this->requestByCurl('https://data.fiixcom.mx/ws/push/send_notification/?auth_key=ul9DXPmiTma8ECGLU0J35V2JG&device_type='.$dispositivo->device_type.'&device_token='.$dispositivo->token.'&message='.$request['descripcion']);
                                }
                            }
                        }
                    }
                }
            }

            $this->requestByCurl("http://data.fiixcom.mx/ws/push/cron_job/?auth_key=ul9DXPmiTma8ECGLU0J35V2JG");
        }
    }

    private function requestByCurl($url)
    {
        $response = "";
        $cURLConnection = curl_init();
        curl_setopt($cURLConnection, CURLOPT_URL, $url);
        curl_setopt($cURLConnection, CURLOPT_HTTPHEADER, array(
            'Accept: application/json',
        ));

        $response = curl_exec($cURLConnection);
        curl_close($cURLConnection);

        return $response;
    }
}
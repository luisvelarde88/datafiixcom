<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Activation_link;
use App\User;

class ActivateController extends Controller
{
    public function userAccount( $token = "" )
    {
        header("Last-Modified: " . gmdate("D, d M Y H:i ") . " GMT"); 
        header("Cache-Control: no-store, no-cache, max-age=0, must-revalidate"); 
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Expires: Sun, 02 Jan 1990 00:00:00 GMT");
        header("Pragma: no-cache");

        $fecha_actual   = strtotime(date("d-m-Y H:i:00",time()));
        $dataToken      = Activation_link::where(["token"=>$token, "status"=>1])->first();
        $dataUser       = null;
        $acction_type   = ( strpos(url()->current(), "resetPasswordAccount") !== false ) ? "R" : "A";

        if( ! empty($dataToken) )
        {
            if( $fecha_actual <= strtotime($dataToken->expiration_at) )
            {
                $dataUser = User::where("id", $dataToken->idusuario)->first();

                if( ! empty($dataUser) )
                {
                    return view('user.change_password', [ 
                        "user_fullname" => strtoupper( utf8_encode($dataUser->name . ' ' . $dataUser->apellidos) ), 
                        "userid" => $dataUser->id,
                        "acction_type" => $acction_type
                    ]);
                }
            }
        }
        return view("user.error_activate_user_account");
    }

    public function userAccountActive()
    {
        return view("user.success_activate_user_account");
    }
}

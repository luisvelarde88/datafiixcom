<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\EmailApi;
use Illuminate\Support\Facades\Mail;
use App\Activation_link;
use App\User;

class UserController extends Controller
{
    public function resetPasswordAccount()
    {
        $formData = request()->all();

        try
        {
            User::where('id', '=', $formData["userid"])->update(array('password' => bcrypt($formData["password"]), 'status' => 1));
            Activation_link::where('idusuario', '=', $formData["userid"])->update(array('status' => 0));

            $dataUser = User::where("id", $formData["userid"])->first();

            $this->send( strtoupper(utf8_encode($dataUser->name . ' ' . $dataUser->apellidos)), $dataUser->email );

            //return redirect()->route('activate.success');
            return view('user.success_activate_user_account');
        } 
        catch (\Throwable $th)
        {
            return redirect()->back()->with(["error_message" => $th->getMessage()]);
        }
    }

    public function send($userfullname, $useremail)
    {
        $myemail = new \stdClass();
        $myemail->sender = 'alarmas@fiixcom.com';
        $myemail->receiver = $userfullname;
 
        Mail::to($useremail)->send(new EmailApi($myemail));
    }
}

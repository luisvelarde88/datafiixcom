<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\cat_codigos;
use App\cat_codigos_master;

class cat_codigos_controller extends Controller
{
    public function add(Request $request)
    {
        $cat_codigos = cat_codigos_master::create($request->all());

        return $cat_codigos;
    }

    public function getAll()
    {
        $cat_codigos = cat_codigos::all();

        return $cat_codigos;
    }

    public function get($id)
    {
        $cat_codigos = cat_codigos::find($id);

        return $cat_codigos;
    }

    public function getMaster($id)
    {
        $cat_codigos = cat_codigos_master::find($id);

        return $cat_codigos;
    }

    public function filter(Request $request)
    {
        $cat_codigos = new cat_codigos();
        $cat_codigos = $cat_codigos->newQuery();

        $filtros = [
            'nombre_protocolo',
            "fecha_add",
            "estado"
        ];

        foreach ($filtros as $filtro)
        {
            if ($request->has($filtro))
            {
                $cat_codigos->where($filtro, $request->input($filtro));
            }
        }

        return $cat_codigos->get();
    }

    public function edit($id, Request $request)
    {
        $cat_codigos = $this->getMaster($id);
        $cat_codigos->fill($request->all())->save();

        return $cat_codigos;
    }

    public function delete($id)
    {
        $cat_codigos = $this->getMaster($id);
        $cat_codigos->delete();

        return $cat_codigos;
    }
}

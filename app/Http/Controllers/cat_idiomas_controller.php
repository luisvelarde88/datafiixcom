<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\cat_idiomas;
use App\cat_idiomas_master;

class cat_idiomas_controller extends Controller
{
    public function add(Request $request)
    {
        $cat_idiomas = cat_idiomas_master::create($request->all());

        return $cat_idiomas;
    }

    public function getAll()
    {
        $cat_idiomas = cat_idiomas::all();

        return $cat_idiomas;
    }

    public function get($id)
    {
        $cat_idiomas = cat_idiomas::find($id);

        return $cat_idiomas;
    }

    public function getMaster($id)
    {
        $cat_idiomas = cat_idiomas_master::find($id);

        return $cat_idiomas;
    }

    public function filter(Request $request)
    {
        $cat_idiomas = new cat_idiomas();
        $cat_idiomas = $cat_idiomas->newQuery();

        $filtros = [
            'idioma',
            'timestamp',
            'estado'
        ];

        foreach ($filtros as $filtro)
        {
            if ($request->has($filtro))
            {
                $cat_idiomas->where($filtro, $request->input($filtro));
            }
        }

        return $cat_idiomas->get();
    }

    public function edit($id, Request $request)
    {
        $cat_idiomas = $this->getMaster($id);
        $cat_idiomas->fill($request->all())->save();

        return $cat_idiomas;
    }

    public function delete($id)
    {
        $cat_idiomas = $this->getMaster($id);
        $cat_idiomas->delete();

        return $cat_idiomas;
    }
}

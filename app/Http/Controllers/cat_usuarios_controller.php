<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\cat_usuarios;
use App\cat_usuarios_master;

class cat_usuarios_controller extends Controller
{
    public function add(Request $request)
    {
        $cat_usuarios = cat_usuarios_master::create($request->all());

        return $cat_usuarios;
    }

    public function getAll()
    {
        $cat_usuarios = cat_usuarios::all();

        return $cat_usuarios;
    }

    public function get($id)
    {
        $cat_usuarios = cat_usuarios::find($id);

        return $cat_usuarios;
    }

    public function getMaster($id)
    {
        $cat_usuarios = cat_usuarios_master::find($id);

        return $cat_usuarios;
    }

    public function filter(Request $request)
    {
        $cat_usuarios = new cat_usuarios_master();
        $cat_usuarios = $cat_usuarios->newQuery();

        $filtros = [
            ['id_cuenta', 	'=', 	"obligatorio"],
            ['usuario', 		'like', ""],
            ['nombre_usuario', 'like', ""],
            ['pass_usuario', 'like', ""],
            ['timestamp', 	'=', 	""],

            ['estado', 		'=', 	"obligatorio"]
        ];

        $opcionales = [];

        foreach ($filtros as $filtro)
        {
            if ($request->has($filtro[0]))
            {
            	if ($filtro[2] == "obligatorio")
            	{
            		if ($filtro[1] == 'like')
                	{
                		$cat_usuarios->where($filtro[0], "like", "%" . $request->input($filtro[0]) . "%");
                	}
                	else
                	{
                		$cat_usuarios->where($filtro[0], $request->input($filtro[0]));
                	}
                }
                else
                {
                	$opcionales[] = $filtro;
            	}
            }
        }

        if ($opcionales)
        {
        	$cat_usuarios->where(function($query) use ($opcionales, $request)
	        {
	        	$primerOpcional = $opcionales[0];

	        	if ($primerOpcional[1] == 'like')
	            {
	            	$query->where($primerOpcional[0], "like", "%" . $request->input($primerOpcional[0]) . "%");
	            }
	            else
	            {
	            	$query->where($primerOpcional[0], $request->input($primerOpcional[0]));
	            }

	            for ($i = 1; $i < count($opcionales); $i++)
	            { 
	            	if ($opcionales[$i][1] == 'like')
	                {
	                	$query->orWhere($opcionales[$i][0], "like", "%" . $request->input($opcionales[$i][0]) . "%");
	                }
	                else
	                {
	                	$query->orWhere($opcionales[$i][0], $request->input($opcionales[$i][0]));
	                }
	            }
	        });
        }   

        return $cat_usuarios->get();
    }

    public function edit($id, Request $request)
    {
        $cat_usuarios = $this->getMaster($id);
        $cat_usuarios->fill($request->all())->save();

        return $cat_usuarios;
    }

    public function delete($id)
    {
        $cat_usuarios = $this->getMaster($id);
        $cat_usuarios->delete();

        return $cat_usuarios;
    }
}

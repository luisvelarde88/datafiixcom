<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\push_devices;
use App\User;
use App\push_allowed_group;
use Carbon\Carbon;
use App\push_group_codes;

class push_devicescontroller extends Controller
{


    public function add(Request $request)
    {
        $dispositivo = push_devices::where([['token','=',$request->token]])->orderBy('id', 'desc')->first();

        if( ! empty($dispositivo->id) ) // ACTUALIZAMOS
        {
            return $this->updatePushDevicesById($dispositivo->id, $request);
        }
        else    // REGISTRAMOS
        {
            $usuario=User::where('email','=',$request->usuario)->first();

            $push_devices = new push_devices;

            $push_devices->id_usuario= $usuario->id;
            $push_devices->token=$request->token;
            $push_devices->status=$request->status;
            $push_devices->device_type=$request->device_type;
            $push_devices->version=$request->version;
            $push_devices->numero_telefonico=$request->numero_telefonico;
            $push_devices->fecha_update=$request->fecha_update;
            $push_devices->save();

            $push_allowed_group= new push_allowed_group;
            $push_allowed_group->id_device=$push_devices->id;
            $push_allowed_group->ids_push_group='1';
            $push_allowed_group->status=1;
            $push_allowed_group->fecha_update=$request->fecha_update;
            $push_allowed_group->save();

            return $push_devices;
        }
    }

    public function get($id)
    {
        $push_devices = push_devices::find($id);

        return $push_devices;
    }

    

    public function edit($id, Request $request)
    {
        $push_devices = $this->get($id);
        $push_devices->fill($request->all())->save();

        return $push_devices;
    }
    public function get_allow_group($id)
    {
        $push_allowed_group = push_allowed_group::find($id);

        return $push_allowed_group;
    }


    public function edit2($token, Request $request)
    {
        $dispositivo=push_devices::where([['token','=',$token],['status','=','ACTIVO']])->orderBy('id', 'desc')->first();
        $grupo=push_allowed_group::where('id_device','=',$dispositivo->id)->first();
        
        $update_grupo=push_allowed_group::where('id',$grupo->id)->update(['ids_push_group'=>$request->ids_push_group,'fecha_update'=>Carbon::now()]);
        
        return explode(',', $request->ids_push_group);
    }

    public function edit_push_devices($token, Request $request)
    {
        $dispositivo = push_devices::where([['token','=',$token]])->orderBy('id', 'desc')->first();

        return $this->updatePushDevicesById($dispositivo->id, $request);
    }



    public function updatePushDevicesById($id, $data)
    {
        $push_devices = new push_devices();
        $push_devices = $push_devices->newQuery();

        $fieldsByUpdate = array();

        $filtros = [
            'id_usuario',
            "token",
            "device_type",
            "version",
            "numero_telefonico",
            "fecha_update",
            "status"
        ];

        if ( ! empty($id) )
        {
            $push_devices->where("id", $id);

            foreach ($filtros as $filtro)
            {
                if ( isset($data[$filtro]) )
                {
                    $fieldsByUpdate[ $filtro ] = $data[$filtro];
                }
            }

            $rows_afected = $push_devices->update( $fieldsByUpdate );
        }

        return json_encode( array( "rows_afected" => $rows_afected ) );
    }



    public function delete($id)
    {
        $push_devices = $this->get($id);
        $push_devices->delete();

        return $push_devices;
    }

    public function filter(Request $request)
    {
        $push_devices = new push_devices();
        $push_devices = $push_devices->newQuery();

        $filtros = [
            'id_usuario',
            "token",
            "device_type",
            "version",
            "numero_telefonico",
            "status"
        ];

        foreach ($filtros as $filtro)
        {
            if ($request->has($filtro))
            {
                $push_devices->where($filtro, $request->input($filtro));
            }
        }

        return $push_devices->get();
    }

    public function desactivar($token)
    {
        $push_devices =push_devices::where('token','=',$token)->orderBy('id', 'desc')->first();
        $push_devices->status='INACTIVO';
        $push_devices->fecha_update=Carbon::now();
        $push_devices->save();
        return $push_devices;
    }


    public function push_codes(){
        $result     = array();
        $push_codes = push_group_codes::all();

        if( !empty($push_codes) ) $result = $push_codes;

        return $result;
    }
}
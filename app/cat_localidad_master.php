<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cat_localidad_master extends Model
{
	public $timestamps = false;
    
    protected $table = 'cat_localidad';

    protected $primaryKey = 'id_loc';

    protected $connection = 'fiixcom_soft-central_master';

	protected $fillable = [
    	'id_loc',
    	'key_mun',
    	"localidad"
    ];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cat_cedulas_master extends Model
{
	public $timestamps = false;

    protected $table = 'cat_cedulas';

    protected $connection = 'fiixcom_soft-central_master';

	protected $fillable = [
    	'id',
    	'id_cuenta',
    	"cedula",
    	'hora_cedula',
    	'hora_evento',
    	"margen",
    	'codigo',
    	'hora_notificacion'
    ];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cat_usuarios_master extends Model
{
	public $timestamps = false;
    
    protected $table = 'cat_usuarios';

    protected $connection = 'fiixcom_soft-central_master';

	protected $fillable = [
    	'id_cuenta',
    	'usuario',
        'nombre_usuario',
        'pass_usuario',
    	'timestamp',
    	'estado'
    ];
}
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cat_estados extends Model
{
	public $timestamps = false;
    
    protected $table = 'cat_estados';

    protected $connection = 'fiixcom_soft-central';

    protected $primaryKey = 'id_edo';

	protected $fillable = [
    	'id_edo',
    	'estado'
    ];
}

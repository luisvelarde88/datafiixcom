<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class push_allowed_group extends Model
{
	public $timestamps = false;
    
    protected $table = 'push_allowed_group';

    protected $connection = 'mysql';

	protected $fillable = [
    	
    
    	'id_device',
    	'id_push_group',
        'status',
        'fecha_update',
        
    ];
}
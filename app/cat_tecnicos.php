<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cat_tecnicos extends Model
{
	public $timestamps = false;
    
    protected $table = 'cat_tecnicos';

    protected $connection = 'fiixcom_soft-central';

	protected $fillable = [
    	'id',
    	'nombre',
    	'apellidos',
    	'puesto',
    	'estado',
    	'whatsapp',
    	'sms',
    	'llamadas',
    	"extension"
    ];
}

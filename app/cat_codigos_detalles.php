<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cat_codigos_detalles extends Model
{
	public $timestamps = false;

    protected $table = 'cat_codigos_detalles';

    protected $connection = 'fiixcom_soft-central';

	protected $fillable = [
    	'id',
    	'id_codigo',
    	"codigo",
    	'nombre',
    	'descripcion',
    	"prioridad",
    	'caracteres_zona',
    	'estado',
    	"timestamp",
    	'restore'
    ];
}

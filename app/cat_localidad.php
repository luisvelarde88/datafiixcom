<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cat_localidad extends Model
{
	public $timestamps = false;
    
    protected $table = 'cat_localidad';

    protected $primaryKey = 'id_loc';

    protected $connection = 'fiixcom_soft-central';

	protected $fillable = [
    	'id_loc',
    	'key_mun',
    	"localidad"
    ];
}

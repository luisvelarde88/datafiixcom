<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cat_cuentas_whatsapp extends Model
{
	public $timestamps = false;

	protected $table = 'cat_cuentas_whatsapp';

    protected $connection = 'fiixcom_soft-central';

	protected $fillable = [
    	'id',
    	'numero_cuenta',
    	"whatsapp",
    	'registro',
    	'fecha',
    	"nombre"
    ];
}

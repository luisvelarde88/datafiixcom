<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class view_eventos_ws extends Model
{
    protected $connection='fiixcom_soft-central';
    protected $table= 'view_eventos_ws';
    protected $fillable=array('id','descripcion','codigo','codigo_prefijo ','cuenta','fecha_recibido','fecha_procesado','zona','llamadas','correos','mensajes','notificado','timestamp','raw','particion','nombre','apellidos','prioridad');   
    public $timestamps = false;
}









<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cat_cuentas extends Model
{
	public $timestamps = false;

    protected $table = 'cat_cuentas';

    protected $connection = 'fiixcom_soft-central';

	protected $fillable = [
    	'id',
    	'cuenta',
    	"nombre",
    	'apellidos',
    	'id_idioma',
    	"particion",
    	'id_protocolo',
    	'id_estado',
    	"id_municipio",
    	'id_localidad',
    	"calle",
    	"numero",
    	"cpostal",
    	"id_colonia",
    	"TelefonoFijo1",
    	'TelefonoFijo2',
    	'TelefonoFijo3',
    	"TelefonoCel1",
    	'TelefonoCel2',
    	"TelefonoCel3",
    	"TelefonoWhatsapp1",
    	"TelefonoWhatsapp2",
    	"TelefonoWhatsapp3",
    	"email",
    	"timestamp",
    	"estado",
    	"cedula",
    	'password',
    	'plataforma'
    ];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cat_cuentas_whatsapp_master extends Model
{
	public $timestamps = false;

	protected $table = 'cat_cuentas_whatsapp';

    protected $connection = 'fiixcom_soft-central_master';

	protected $fillable = [
    	'id',
    	'numero_cuenta',
    	"whatsapp",
    	'registro',
    	'fecha',
    	"nombre"
    ];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cat_cuentas_sms_master extends Model
{
    //
    protected $connection='fiixcom_soft-central_master';
    protected $table= 'cat_cuentas_sms';
    protected $fillable=array('numero_cuenta','sms','registro','fecha','nombre');   
    public $timestamps = false;
}

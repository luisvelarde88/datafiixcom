<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cat_idiomas extends Model
{
	public $timestamps = false;
    
    protected $table = 'cat_idiomas';

    protected $connection = 'fiixcom_soft-central';

	protected $fillable = [
    	'id',
    	'idioma',
    	'timestamp',
    	'estado'
    ];
}

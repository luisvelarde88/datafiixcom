<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cat_estados_master extends Model
{
	public $timestamps = false;
    
    protected $table = 'cat_estados';

    protected $connection = 'fiixcom_soft-central_master';

    protected $primaryKey = 'id_edo';

	protected $fillable = [
    	'id_edo',
    	'estado'
    ];
}

<div style="font-family: arial;color: #343434;">
    <div style="color: white;text-align: center;padding: 50px 20px 50px 20px;margin: 0;background: url('https://www.fiixcom.mx/es/img/principal_fiixcom_shdw2.jpg');background-repeat: no-repeat !important;background-size: cover !important;height: 100%;background-color: #343434;">
        <img src="https://www.fiixcom.mx/es/img/FIIXCOM-BLANCO.png" alt="LOGO_FIIXCOM" style="width: 80%;">
        
    </div>
    <div style="margin: 0;text-align: center;padding: 0 20px 10px 20px;">
        <h1>¡CUENTA ACTIVADA!</h1>
        <h4>
            {{ $emailData->receiver }}, usted ha activado su cuenta de forma correcta.
            <br>
            <br>
            Para iniciar sesión use este correo y la contraseña que utilizo para activar su cuenta.
        </h4>
    </div>
    <div style="background-color: #343434;color: white;padding: 10px 20px;">
        Favor de no responder este mensaje porque no obtendrá respuesta.
    </div>
</div>
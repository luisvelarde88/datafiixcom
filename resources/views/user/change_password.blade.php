<!DOCTYPE html>
<html lang="es">
<head>
<title>Facturacion Electronica - FIIXCOM Soluciones &#174</title>
        <meta charset="utf-8">
        <meta name="google-site-verification" content="flodLMiNpqpnpjTalu8JxaE-zODC3T6cgdmFUSjoOMY" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Iconos -->
        <link rel="shortcut icon" href="https://www.fiixcom.mx/es/img/fiixcom.ico">
		<link rel="alternate" hreflang="es-MX" href="https://www.fiixcom.mx/es/">
				<link rel="apple-touch-icon" sizes="57x57" href="https://www.fiixcom.mx/es/img/ico/apple-touch-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="https://www.fiixcom.mx/es/img/ico/apple-touch-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="https://www.fiixcom.mx/es/img/ico/apple-touch-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="https://www.fiixcom.mx/es/img/ico/apple-touch-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="https://www.fiixcom.mx/es/img/ico/apple-touch-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="https://www.fiixcom.mx/es/img/ico/apple-touch-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="https://www.fiixcom.mx/es/img/ico/apple-touch-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="https://www.fiixcom.mx/es/img/ico/apple-touch-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="https://www.fiixcom.mx/es/img/ico/apple-touch-icon-180x180.png">
		<link rel="icon" type="image/png" href="https://www.fiixcom.mx/es/img/ico/favicon-16x16.png" sizes="16x16">
		<link rel="icon" type="image/png" href="https://www.fiixcom.mx/es/img/ico/favicon-32x32.png" sizes="32x32">
		<link rel="icon" type="image/png" href="https://www.fiixcom.mx/es/img/ico/favicon-96x96.png" sizes="96x96">
		<link rel="icon" type="image/png" href="https://www.fiixcom.mx/es/img/ico/android-chrome-192x192.png" sizes="192x192">
		<meta name="msapplication-square70x70logo" content="https://www.fiixcom.mx/es/img/ico/smalltile.png" />
		<meta name="msapplication-square150x150logo" content="https://www.fiixcom.mx/es/img/ico/mediumtile.png" />
		<meta name="msapplication-wide310x150logo" content="https://www.fiixcom.mx/es/img/ico/widetile.png" />
		<meta name="msapplication-square310x310logo" content="https://www.fiixcom.mx/es/img/ico/largetile.png" />
		<meta name="apple-mobile-web-app-title" content="FIIXCOM">
		<meta name="application-name" content="FIIXCOM WEB">
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Exo+2" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Ruda" rel="stylesheet"> 
        <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
        <!-- Metatags Generales -->
        <meta property="description" content="FIIXCOM es Tecnología, estamos en Mazatlan y Culiacan y ofrecemos, Desarrollo de Software a la medida, Estrategias en Marketing Web, Desarrollo de Sistemas,SEO, SEM,Tienda en linea, E-commerce, Programas, Diseño Web, Paginas Web, Tienda en Linea, Gestor de Contenidos, CMS, Blogs, Noticieros"/>	
        <meta itemprop="name" content="Diseño, Desarrollo de Paginas Web, Estrategias Web y Sistemas a la medida"/>
        <meta itemprop="title" content="Desarrollo de Software"/>
        <meta itemprop="description" content="FIIXCOM es Tecnología, estamos en Mazatlan y Culiacan y ofrecemos, Desarrollo de Software a la medida, Estrategias en Marketing Web, Desarrollo de Sistemas,SEO, SEM,Tienda en linea, E-commerce, Programas, Diseño Web, Paginas Web, Tienda en Linea, Gestor de Contenidos, CMS, Blogs, Noticieros"/>
        <meta itemprop="url" content='https://www.fiixcom.mx'/>
        <script src="https://wchat.freshchat.com/js/widget.js"></script>
                		<meta itemprop="MakesOffer" content="Facturación Electrónica">
                        		<meta itemprop="MakesOffer" content="Sitios Web">
                        		<meta itemprop="MakesOffer" content="Marketing Web">
                        		<meta itemprop="MakesOffer" content="Aplicaciones Móviles">
                        		<meta itemprop="MakesOffer" content="Desarrollo de Software">
                        		<meta itemprop="MakesOffer" content="Consultoría TI">
                        		<meta itemprop="MakesOffer" content="E-Commerce">
                        		<meta itemprop="MakesOffer" content="Paginas">
                        		<meta itemprop="MakesOffer" content="Diseño">
                        		<meta itemprop="MakesOffer" content="SEO">
                        		<meta itemprop="MakesOffer" content="SEM">
                        		<meta itemprop="MakesOffer" content="Tiendas en Linea">
                        		<meta itemprop="MakesOffer" content="Timbres">
                        		<meta itemprop="MakesOffer" content="Folios">
                        		<meta itemprop="MakesOffer" content="Integraciones">
                        		<meta itemprop="MakesOffer" content="CMS">
                        		<meta itemprop="MakesOffer" content="Gestor de contenido">
                        <meta name="keywords" content="Facturacion Electronica, Software, Paginas Web, Diseño Web, Marketing Web,  E-Commerce,ERP, Consultoría TI, SEO, SEM, Tienda en Linea, Gestor de Contenidos, CMS, Blogs, Noticieros
	mazatlan, sinaloa, mexico, monterrey, nuevo leon, paginas web mazatlan, paginas web monterrey, sistemas mazatlan, sistemas monterrey, Diseño web mazatlan, Diseño web monterrey,
	desarrollo de sistemas mazatlan, desarrollo de sistemas monterrey, erp mazatlan, erp monterrey, facturacion electronica mazatlan, facturacion electronica monterrey,
	estrategia web mazatlan, estrategia web monterrey, seo web mazatlan, seo mazatlan, seo web monterrey, seo monterrey, sem web monterrey, sem web mazatlan,
	marketing web mazatlan, marketing web monterrey, marketing digital mazatlan, marketing digital monterrey, consultoria mazatlan, consultoria monterrey,
	aplicaciones moviles mazatlan, app movil mazatlan, android mazatlan, ios mazatlan, aplicaciones moviles monterrey, app movil monterrey, android monterrey,
	ios monterrey, app mazatlan, app monterrey"/>
        <meta name="description" content="FIIXCOM es Tecnología, estamos en Mazatlan y Culiacan y ofrecemos, Desarrollo de Software a la medida, Estrategias en Marketing Web, Desarrollo de Sistemas,SEO, SEM,Tienda en linea, E-commerce, Programas, Diseño Web, Paginas Web, Tienda en Linea, Gestor de Contenidos, CMS, Blogs, Noticieros"/>
        <meta name="original-source" content="https://www.fiixcom.mx/es/software">
        <meta name="robots" content="index, follow, NOODP"/>
        <meta name="distribution" content="global"/>
        <meta name="rating" content="general"/>
        <meta name="language" content="es_ES"/>
        <meta name="Author" content="FIIXCOM">
        <meta name="organization" content="FIIXCOM">
        <meta name="GENERATOR" content="FIIXCOM">
        <meta name="Googlebot" content="all">
        <meta name="revisit-after" content="1 days">
        <meta name="classification" content="Tecnologia">
        <meta http-equiv="Refresh" content="600" />
        <!-- Especificaciones Social Sharing -->
        <meta property="og:title" content="Software - FIIXCOM Soluciones" /> 
        <meta property="og:type" content="website" />
        <meta property="og:url" content="https://www.fiixcom.mx/es/software" />
        <meta property="og:description" content="FIIXCOM es Tecnología, estamos en Mazatlan y Culiacan y ofrecemos, Desarrollo de Software a la medida, Estrategias en Marketing Web, Desarrollo de Sistemas,SEO, SEM,Tienda en linea, E-commerce, Programas, Diseño Web, Paginas Web, Tienda en Linea, Gestor de Contenidos, CMS, Blogs, Noticieros" />  
        <meta property="og:image" content="https://www.fiixcom.mx/es/img/social-sharing/software.jpg" />
		<meta property="og:image:width" content="450" />
		<meta property="og:image:height" content="299" />
        <meta property="og:locale" content="es_LA" />
        <meta property="og:site_name" content="FIIXCOM" />
        <meta property="fb:app_id" content="595962843935810" />
        <!-- Twitter Cards -->
        <meta name="twitter:card" content="summary_large_image">
        <meta name="twitter:title" content="FIIXCOM Soluciones">		
        <meta name="twitter:site" content="@fiixcom">
        <meta name="twitter:creator" content="FIIXCOM">
        <meta name="twitter:domain" content="https://www.fiixcom.com/">
        <meta name="twitter:url" content="https://twitter.com/fiixcom">
        <meta name="twitter:description" content="Diseño, Desarrollo de Paginas Web, Estrategias Web y Sistemas a la medida">	
        <meta name="twitter:image" content="https://www.fiixcom.mx/es/img/social-sharing/software.jpg">
        <!-- CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="https://www.fiixcom.mx/es/css/style.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://www.fiixcom.mx/es/css/hamburgers.css" type="text/css">
        <!-- <link rel="stylesheet" href="https://www.fiixcom.mx/es/css/advanced-slider-base.css" type="text/css"> -->
        <!-- <link rel="stylesheet" href="https://www.fiixcom.mx/es/css/responsive-slider.css" type="text/css"> -->
        <link rel="stylesheet" href="https://www.fiixcom.mx/es/css/jquery.simplyscroll.css" type="text/css">
        <link rel="stylesheet" href="https://www.fiixcom.mx/es/css/animations.css" type="text/css">
        <link rel="stylesheet" href="https://www.fiixcom.mx/es/css/style-subnav.css" type="text/css">      
        <link rel="stylesheet" href="https://www.fiixcom.mx/es/css/flexslider.css" type="text/css" media="screen" />
        <!-- <link rel="stylesheet" href="https://www.fiixcom.mx/es/css/slider-portafolios.css" type="text/css" media="screen" /> -->
        <link rel="stylesheet" href="https://www.fiixcom.mx/es/css/magnific-popup.css">
        <!-- <link rel="stylesheet" href="https://www.fiixcom.mx/es/css/lightslider.css" type="text/css" /> -->
        <!-- JS -->
        <script src='https://www.google.com/recaptcha/api.js'></script>
        <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="https://www.fiixcom.mx/es/js/jquery.simplyscroll.js" type="text/javascript"></script>
        <script src="https://www.fiixcom.mx/es/js/javascript.js" type="text/javascript"></script>
        <script src="https://www.fiixcom.mx/es/js/slider.js" type="text/javascript"></script> 
        <script src="https://www.fiixcom.mx/es/js/anclas.js" type="text/javascript"></script> 
        <!-- <script src="https://www.fiixcom.mx/es/js/jssor.slider-22.0.6.mini.js" type="text/javascript"></script> -->
        <script src="https://www.fiixcom.mx/es/js/jquery.magnific-popup.js"></script>
        <!-- <script src="https://www.fiixcom.mx/es/js/lightslider.js" type="text/javascript"></script> -->

    <style>
        .bg-dark {
            background-color: black !important;
        }
        .navbar-brand img{
            max-height: 40px;
        }
        .btn{
            width: 100%;
            padding: 6px 12px;
            height: auto;
        }
    </style>
</head>

<body>
    <nav class="navbar navbar-default navbar-fixed-top" id="navbarMain" style="height:58px">
        <div class="navbar-header">
            <div class="logo">
                <a itemprop="url" class="" href="https://www.fiixcom.mx/es/"><img itemprop="image" id="fiixcomNavbarLogo" src="https://www.fiixcom.mx/es/img/fiixcom-logo-menu.png"></a>
            </div>
        </div>
    </nav>

    <div class="container" style="margin-top: 40px">
        <form method="post" name="formData" id="formData" action="{{url('user/resetPasswordAccount')}}">
            {{ csrf_field() }}
            <input type="hidden" name="userid" value="{{$userid}}">
        </form>
        <br><br><br>

        @if($acction_type == 'A')
        <div class="row">
            <div class="col-md-6 col-md-offset-3 col-fact-2" style="text-align:center;">
                <h2 class="">Bienvenido, al proceso de activación</h2>
                <h3 class="">{{$user_fullname}}</h3>
                <p>
                    Para continuar con la activación de tu cuenta es necesario que realices una nueva contraseña,
                    para ello llena los siguientes campos.
                </p>
            </div>
        </div>
        @else
        <div class="row">
            <div class="col-md-6 col-md-offset-3 col-fact-2" style="text-align:center;">
                <h2 class="">Bienvenido, al proceso de recuperación</h2>
                <h3 class="">{{$user_fullname}}</h3>
                <p>
                    Para continuar con la recuperación de tu cuenta es necesario que realices una nueva contraseña,
                    para ello llena los siguientes campos.
                </p>
            </div>
        </div>
        @endif

        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="form-group">
                    <input type="password" name="password" id="txtPass" placeholder="Contraseña" class="form-control" required form="formData">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="form-group">
                    <input type="password" name="pass_conf" id="txtPassConf" placeholder="Confirmar contraseña" class="form-control" required form="formData">
                </div>
            </div>
        </div>
        @if(empty(Session::get('error_message')))
            <div class="row" id="alertMessage" style="display: none;">
                <div class="col-md-6 col-md-offset-3">
                    <div class="alert alert-warning alert-dismissible fade show" role="alert">
                        <strong>Atención!</strong> <span id="alertMessageText"></span>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            </div>
        @else
            <div class="row" id="alertMessage">
                <div class="col-md-6 col-md-offset-3">
                    <div class="alert alert-warning alert-dismissible fade show" role="alert">
                        <strong>Atención!</strong> <span id="alertMessageText">{{Session::get('error_message')}}</span>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            </div>
        @endif
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <button type="submit" id="btnActivarCuenta" class="btn btn-success" form="formData">ENVIAR</button>
            </div>
        </div>
        <br><br><br>
    </div>

    <footer class="footer text-center">
        <!-- Begin definition of footer-up section -->
        <section class="footer-up">
            <div class="container">
                <div class="row">
               		<div class="col-sm-4">
               			<h3>¿Ya estás listo para comenzar la experiencia?</h3>
               			<p>Dá el primer paso, no importa que tu idea no sea clara, nosotros te apoyamos.</p>
               			<a onclick="goog_report_conversion ('https://www.fiixcom.mx/es/contacto/?ref=ready')" href="https://www.fiixcom.mx/es/contacto/?ref=ready">Platícanos sobre tu proyecto <i class="fa fa-caret-right" aria-hidden="true"></i></a>
               		</div>
               		<div class="col-sm-4">
               			<h3>¿Eres cliente?</h3>
               			<p>Dínos cómo podemos ayudarte de forma puntual, nos encantaría ayudarte.</p>
               			<a onclick="goog_report_conversion ('https://www.fiixcom.mx/es/contacto/?ref=cli')" href="https://www.fiixcom.mx/es/contacto/?ref=cli">Pídenos ayuda <i class="fa fa-caret-right" aria-hidden="true"></i></a>
               		</div>
               		<div class="col-sm-4">
               			<h3>¿No encontraste lo que buscabas?</h3>
               			<p>Cuéntanos esa parte que no encontraste en nuestro sitio, queremos saber más.</p>
               			<a onclick="goog_report_conversion ('https://www.fiixcom.mx/es/contacto/?ref=miss')" href="https://www.fiixcom.mx/es/contacto/?ref=miss">Contáctanos <i class="fa fa-caret-right" aria-hidden="true"></i></a>
               		</div>
                </div>
            </div>       
        </section>
        <!-- End definition of footer-top section -->
        <section class="footer-down">
            <div class="container end-page">
                <div class="row">
                    <div class="col-md-3 alinear social">
                        <h5>Síguenos en redes sociales</h5>
                        <ul class="redes-sociales">
                            <li class="red-social">
                                <a href="https://www.facebook.com/FIIXCOM/" target="_blank">
                                    <img class="social-icon" src="https://www.fiixcom.mx/es/img/iconos/facebook.png">
                                    <span class="no-show">Facebook</span>
                                </a>
                            </li>
                            <li class="red-social">
                                <a href="https://www.instagram.com/fiixcom/" target="_blank">
                                    <img class="social-icon" src="https://www.fiixcom.mx/es/img/iconos/instagram.png">
                                    <span class="no-show">Instagram</span>
                                </a>
                            </li>
                            <li class="red-social">
                                <a href="whatsapp://send?text=¡Hola!%20Te%20Recomiendo%20que%20visites%20https://fiixcom.com">
                                    <img class="social-icon" src="https://www.fiixcom.mx/es/img/iconos/whatsapp.png">
                                    <span class="no-show" target="_blank">Whatsapp (6691-61-61-18)</span>
                                </a>
                            </li>
                            <li class="red-social">
                                <a href="https://twitter.com/fiixcom" target="_blank">
                                    <img class="social-icon" src="https://www.fiixcom.mx/es/img/iconos/twitter.png">
                                    <span class="no-show">Twitter</span>
                                </a>
                            </li>          
                            <li class="red-social">
                                <a href="https://plus.google.com/+Fiixcom" target="_blank">
                                    <img class="social-icon" src="https://www.fiixcom.mx/es/img/iconos/google-plus.png">
                                    <span class="no-show">Google Plus</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-2 alinear">
                   		<ul class="footer-list">
                   		    <li class="dropdown-header" onclick="javascript:toggleList('software-list')"><h5>Software</h5><span id="chev-software-list" class="fa fa-chevron-down chevron-down" aria-hidden="true"></span></li>
                   			<ul id="software-list" class="no-visible">
                                <li><a href="https://www.fiixcom.mx/es/software/facturacion-electronica/" target="_self">Facturación Electrónica</a></li>
                                <li><a href="https://www.fiixcom.mx/es/software/sitios-web/" target="_self">Sitios Web</a></li>
								<li><a href="https://www.fiixcom.mx/es/software/marketing-web/" target="_self">Marketing Web</a></li>
                                <li><a href="https://www.fiixcom.mx/es/software/aplicaciones-moviles/" target="_self">Aplicaciones Móviles</a></li>
                                <li><a href="https://www.fiixcom.mx/es/software/desarrollo-de-software/" target="_self">Desarrollo de Software</a></li>
                                <li><a href="https://www.fiixcom.mx/es/software/consultoria-ti/" target="_self">Consultoría TI</a></li>
		                    </ul>
        	            </ul>
                        <ul class="footer-list">
                            <li class="dropdown-header" onclick="javascript:toggleList('soporte-list')"><h5>Soporte</h5><span id="chev-soporte-list" class="fa fa-chevron-down chevron-down" aria-hidden="true"></span></li>
                            <ul id="soporte-list" class="no-visible">
                                <li><a href="https://www.fiixcom.mx/es/soporte/soporte-en-sitio/" target="_self">Soporte en sitio</a></li>
                                <li><a href="https://www.fiixcom.mx/es/soporte/poliza-de-soporte/" target="_self">Pólizas de Soporte</a></li>
                                <li><a href="https://www.fiixcom.mx/es/soporte/centro-de-servicio/" target="_self">Centro de Servicio</a></li>
                            </ul>
                        </ul>
                    </div>
                    <div class="col-md-2 alinear">
	                    <ul class="footer-list">
	                   		<li class="dropdown-header" onclick="javascript:toggleList('redes-list')"><h5>Redes</h5><span id="chev-redes-list" class="fa fa-chevron-down chevron-down" aria-hidden="true"></span></li>
                        	<ul id="redes-list" class="no-visible">
                                <li><a href="https://www.fiixcom.mx/es/redes/cableado-estructurado/" target="_self">Cableado Estructurado</a></li>
                                <li><a href="https://www.fiixcom.mx/es/redes/enlaces-inalambricos/" target="_self">Enlaces Inalámbricos</a></li>
                                <li><a href="https://www.fiixcom.mx/es/redes/centro-de-datos/" target="_self">Centros de Datos</a></li>
                                <li><a href="https://www.fiixcom.mx/es/redes/control-y-admon-de-trafico-de-red/" target="_self">Control de tráfico de Red</a></li>
                                <li><a href="https://www.fiixcom.mx/es/redes/marcas-y-equipos/" target="_self">Marcas y Equipos</a></li>
                                <li><a href="https://www.fiixcom.mx/es/redes/soluciones-voip/" target="_self">Soluciones VoIP</a></li>
                                <li><a href="https://www.fiixcom.mx/es/redes/diseno-e-instalacion-de-red/" target="_self">Diseño e Instalación de Red</a></li>
                    		</ul>
	                    </ul>
                    </div>
                    <div class="col-md-2 alinear">
                   		<ul class="footer-list">
                   			<li class="dropdown-header" onclick="javascript:toggleList('seguridad-list')"><h5>Seguridad</h5><span id="chev-seguridad-list" class="fa fa-chevron-down chevron-down" aria-hidden="true"></span></li>
                        	<ul id="seguridad-list" class="no-visible">
                                <li><a href="https://www.fiixcom.mx/es/seguridad/cctv/" target="_self">CCTV</a></li>
                                <li><a href="https://www.fiixcom.mx/es/seguridad/alarmas/" target="_self">Alarmas</a></li>
                                <li><a href="https://www.fiixcom.mx/es/seguridad/proteccion-perimetral/" target="_self">Protección Perimetral</a></li>
                                <li><a href="https://www.fiixcom.mx/es/seguridad/equipo-contra-incendios/" target="_self">Equipos contra Incendios</a></li>
                                <li><a href="https://www.fiixcom.mx/es/seguridad/controles-de-acceso/" target="_self">Controles de Acceso</a></li>
                            </ul>
                       </ul>
                    </div>
                    <div class="col-md-2 alinear">
                        <ul class="footer-list">
                    			<li class="dropdown-header" onclick="javascript:toggleList('gps-list')"><h5>GPS</h5><span id="chev-gps-list" class="fa fa-chevron-down chevron-down" aria-hidden="true"></span></li>
                        		<ul id="gps-list" class="no-visible">
                                    <li><a href="https://www.fiixcom.mx/es/gps/servicios/" target="_self">Servicios</a></li>
                                    <li><a href="https://www.fiixcom.mx/es/gps/industrias" target="_self">Industria</a></li>
                                    <li><a href="https://www.fiixcom.mx/es/gps/soporte/" target="_self">Soporte</a></li>
                                    <li><a href="https://www.fiixcom.mx/es/gps/plataforma/" target="_self">Plataforma</a></li>
                    			</ul>
                    	</ul>
                        <ul class="footer-list">
                   		<li class="dropdown-header" onclick="javascript:toggleList('energia-list')"><h5>Energía</h5><span id="chev-energia-list" class="fa fa-chevron-down chevron-down" aria-hidden="true"></span></li>
                   			<ul id="energia-list" class="no-visible">
                    			<li><a href="https://www.fiixcom.mx/es/energia/tierra-fisica/" target="_self">Tierra Física</a></li>
                                <li><a href="https://www.fiixcom.mx/es/energia/paneles-solares/" target="_self">Páneles Solares</a></li>
                    		</ul>
                        </ul>
                    </div>
                </div>
                <div class="row">
               		<div class="div-leyenda">
                    	<i>No estamos aqui para cambiar la mente, más bien, el universo. JHG</i>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3 info-fiixcom">                                   
                        <div class="row"><h5>Derechos Reservados FIIXCOM ©  2020</h5></div>
                    </div>
                    <div class="col-sm-5 aviso-site">
                        <div class="row">
                            <h6 class="link-footer">
                                <a class="contactanos" href="https://www.fiixcom.mx/es/sitemap" target="_self">MAPA DEL SITIO</a>
                                <span class="linea-ver"></span>
                                <a href="https://www.fiixcom.mx/es/avisoprivacidad/" class="politica-privacidad">POLÍTICA DE PRIVACIDAD</a>
                                <span class="linea-ver"></span>
                                <a class="contactanos" href="https://www.fiixcom.mx/es/partners/">PARTNERS</a>
                                <span class="linea-ver"></span>
                                <a class="contactanos" onclick="goog_report_conversion ('https://www.fiixcom.mx/es/contacto/')" href="https://www.fiixcom.mx/es/contacto/">CONTÁCTANOS</a>
                            </h6>
                        </div>
                    </div>
                    <div class="col-sm-4 logo-footer">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row"><span>&nbsp;</span></div>
                                <div class="row">
                                    <div><img alt="Logo TRADENCOM" src="https://www.fiixcom.mx/es/img/tradencom.png"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row"><span>Impulsado por:</span></div>
                                <div class="row">
                                    <div><img src="https://www.fiixcom.mx/es/img/FIIXCOM-BLANCO.png" alt="Logo FIIXCOM"></div>
                                </div>
                            </div>
                        </div>    
                    </div>
                </div>
            </div>
            <!-- AVISO DE PRIVACIDAD -->     
            <div id="mensaje" class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <section class="contenidoCentrado"> 
                            <div class="container ">
                                <div class="row">
                                    <div class="col-md-12 col-lg-12" style="text-align: left;">
                                        <article>
                                            <center><h2><strong>FIIXCOM</strong></h2></center>
                                            <center><h2>Política de Privacidad</h2></center>
                                            <br> <br>
                                            <h3>¿Qué hacemos con tu información?</h3>
                                            <br>
                                            <p></p><p>1.El término “información personal” como es usado aquí, es definido como cualquier información que identifique o que pueda ser usada para identificar, contactar o localizar a la persona a la que la información pertenece. La información personal que recolectamos estará sujeta a esta política de privacidad, y será modificada cada cierto tiempo.</p>
                                            <p>2.  Cuando te registras por FIIXCOM pedimos tu dirección de correo electrónico.</p>
                                            <p>3.  FIIXCOM usa la información que recolectamos para los siguientes propósitos generales: provisión de productos y servicios, facturación, identificación y autenticación, mejora de servicios, contactos e investigación.</p>
                                            <p>4.  Como parte del proceso de compra y venta de FIIXCOM, obtendrás la dirección de correo electrónico y/o la dirección de envío de tus clientes. Al entrar a nuestro Acuerdo de Usuario, estarás de acuerdo, con el respeto a otro usuarios que la información personal que obtengas a través de FIIXCOM o a través de FIIXCOM comunicación relacionada o FIIXCOM, transacción facilitada, FIIXCOM te otorga una licencia para usar información solo para FIIXCOM comunicaciones relacionadas que no sean mensajes comerciales no solicitados, FIIXCOM. No tolera spam. Por lo tanto, sin limitar el precedente, no estás licenciado para añadir el nombre de alguien que ha comprado un producto a tu lista de correos (electrónico o físico), sin su consentimiento.</p>

                                            <h4>LO CUAL SIGNIFICA</h4>
                                            <p>Cuando te registras, tú provees información personal, la cual nosotros recolectamos y usamos. Para el proceso de pago también chequeamos la información de tu tarjeta de crédito. Solo usamos tu FIIXCOM información del cliente para FIIXCOM comunicaciones relacionadas, a menos que les des permiso para hacer lo contrario. No spamees a nadie.</p>
                                            <h3>Seguridad</h3>
                                            <p>La seguridad de tu información personal es importante para nosotros. Cuando tu registras información sensible, como el número de tarjeta de crédito en nuestro formulario de registro, encriptamos la transmisión de esa información usando una tecnología de capas de enchufes seguros, mejor conocida como SSL. Los detalles de las tarjetas de crédito son almacenados y encriptados usando encriptación de AES-256. Como un dócil proveedor de servicios de nivel 1 PCI-DSS, seguimos todos los requerimientos de PCI-DSS y los implementos adicionales generalmente aceptamos por los estándares de la industria para proteger la información personal que es entregada a nosotros, tanto en la transmisión como cuando la recibimos. Ningún método de transmisión en la Internet, o algún método de almacenamiento electrónico es 100% seguro. Por lo tanto, mientras luchamos por usar medios comercialmente aceptables para proteger tu información personal, no podemos garantizar su absoluta seguridad. Si tienes alguna pregunta sobre la seguridad de nuestro sitio web, puedes enviarnos un correo electrónico a joelhernandez@fiixcom.com.</p>
                                            <h4>LO CUAL SIGNIFICA</h4>
                                            <p>Encriptaremos la información de tu tarjeta de crédito y cualquier otra información sensible de acuerdo a los estándares de la industria. Como las cosas suelen pasar, no podemos garantizar una seguridad al 100% de tu información. Si tienes preguntas, envíanos un correo electronic a joelhernandez@fiixcom.com.</p>
                                            <h3>Divulgación</h3>
                                            <p>1.  FIIXCOM puede usar a terceros como proveedores de servicios para que provean ciertos servicios a ti, y con los cuales podemos compartir cierta información personal. Como requerimiento, le pedimos a cualquier compañía con la que podamos compartir alguna información personal, que protejan la información de un amanera consistente con su póliza y que limiten el uso de dicha información en el desempeño de los servicios para FIIXCOM.</p>
                                            <p>2.  FIIXCOM puede divulger información personal bajo circunstancias personales, como para cumplir con órdenes de la corte que requieren entregar información de ese tipo o cuando tus acciones violen los términos de servicio.</p>
                                            <p>3.  No vendemos o proveemos información personal a otras compañías para el mercadeo de sus propios productos y servicios.</p>
                                            <h4>LO CUAL SIGNIFICA</h4>
                                            <p>En ciertas circunstancias, podemos divulgar tu información personal, como en órdenes de la corte.</p>
                                            <h3>Almacenamiento de la información del cliente</h3>
                                            <p>A FIIXCOM le pertenece el almacenamiento de la información, las bases de datos y todos los derechos a la aplicación de FIIXCOM. Sin embargo, no reclamamos ningún derecho sobre tu información. Todos los derechos de tu información te pertenecen y nunca contactaremos a tus clientes directamente o usaremos tú información para alguna ventaja de nuestro negocio o para competir contigo o apuntar nuestro mercado a tus clientes.</p>
                                            <h4>LO CUAL SIGNIFICA</h4>
                                            <p>Tu información te pertenece y respetamos eso. No trataremos de competir contigo o de escribirles a tus clientes.</p>
                                            <h3>Cookies</h3>
                                            <p>Una cookie en una pequeña cantidad de información, que puede incluir un identificador anónimo y único. Las cookies son enviadas a tu buscador desde un sitio web y almacenadas en el disco duro de tu computadora. Toda computadora que accese a nuestro sitio web, le es asignada una cookie diferente.</p>
                                            <h3>Remercadeo y análisis de Google</h3>
                                            <p>Usamos un servicio proveido por Google llamado Google Analitycs (GA). GA nos permite alcanzar a la gente que ha visitado previamente nuestro sitio y enseñarles anuncios relevantes cuando visiten otro sitio en la Internet en la Google Display Network. Esto es a menudo llamado remercadeo.
                                            Las Cookies son usadas para rastrear la sesión de tu página web, para servir anuncios personalizados de Google y otras terceras partes. Cuando visitas este sitio web, puedes ver anuncios puestos en el sitio por Google o alguna otra tercera parte. A través de las cookies de primera y tercera mano, estas terceras partes pueden recolectar información sobre ti mientras visitas su sitio web y otros sitios web. Puede que usen esa información para mostrarte anuncios en este sitio web y a través de toda la Internet basados en tus visitas anteriores a este sitio web y a otros en la Internet. Nosotros no recolectamos esta información o controlamos el contenido de loa anuncios que ves.</p>
                                            <h3>Excluir</h3>
                                            <p>Puede que seas capaz de excluirte de los anuncios personalizados de Google Display Network visitando el administrador de preferencias de anuncios (http://www.google.com/ads/preferences/), and the Google Analytics Opt-out Browser Add-on (http://www.google.ca/ads/preferences/plugin/).
                                            Tu uso de este sitio web sin excluirte significa que entiendes y estás de acuerdo con la recolección de información y de que esta te provea con los anuncios de remercadeo usando GA y cookies de una tercera parte basados en tus visitas anteriores y en otras partes de la Internet.</p>
                                            <h4>LO CUAL SIGNIFICA</h4>
                                            <p>Para identificarte electrónicamente, una cookie será almacenada en tu computadora. Tenemos una herramienta de remercadeo que nos permite tomar notas de tus vistas a nuestro sitio web y así, mostrarte anuncios relevantes en nuestro sitio web y a través de la Internet. Siempre puedes anular esta opción.</p>
                                            <h3>PCI-DSS</h3>
                                            <p>El Payment Card Industry Data Security Standard (PCI-DSS) o el estándar de seguridad de información de la industria de pago de tarjetas, es un set de requerimientos de seguridad manejados por el Consejo de Estándares de Seguridad PCI, un esfuerzo conjunto de las marcas d epago, incluyendo Visa, MasterCard, American Express y Discover. Los requerimientos de PCI-DSS ayudan a asegurar la seguridad de dar información de tarjetas de crédito por parte de comerciantes y proveedores de servicios.</p>
                                            <h4>LO CUAL SIGNIFICA</h4>
                                            <p>Usaremos los estándares en seguridad de la industria, los mismos usados por las grandes compañías de tarjetas de crédito, para ayudarte a mantener una segura cuenta de FIIXCOM.
                                            </p><h3>Cambios a esta Política de Privacidad</h3>
                                            <p>Nos reservamos el derecho de modificar esta declaración de privacidad en cualquier momento, por lo que debes revisarla frecuentemente, por favor. Si hacemos algunos cambios a esta política, te notificaremos aquí o a través de nuestra página para que seas consciente de la información que recolectamos, cómo la usamos y bajo qué circunstancias, si hay algunas, decidimos divulgarla.</p>
                                            <h4>LO CUAL SIGNIFICA</h4>
                                            <p>Podemos cambiar esta Declaración de Privacidad. Si es un gran cambio, te informaremos aquí mismo.</p>
                                            <h3>Preguntas</h3>
                                            <p>Cualquier pregunta sobre esta Política de Privacidad debería ser enviada  por correo electrónico a:
                                            servicios@fiixcom.com</p><br><br>
                                            <p></p>        			
        		                        </article>
        	                        </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>            
        </section>     
    </footer>

    <script>
        $(function(){

            $("#formData").submit(function(e){

                var first_password = $("#txtPass").val(),
                    second_password = $("#txtPassConf").val(),
                    message_error = null;

                if( first_password == "" || first_password == null || first_password == undefined )
                {
                    message_error = "las credenciales no pueden estar vacías, favor de corregir para continuar.";
                }
                else if( first_password !== second_password )
                {
                    message_error = "las credenciales no son idénticas, favor de corregir para continuar.";
                }

                if( message_error != null )
                {
                    $("#alertMessageText").html( message_error );
                    $("#alertMessage").show(200);

                    e.preventDefault();

                }
            });

        });

    </script>

</body>

</html>